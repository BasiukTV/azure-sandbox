from opentelemetry import metrics, trace
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader
from azure.monitor.opentelemetry.exporter import AzureMonitorMetricExporter
from opentelemetry._logs import set_logger_provider
from opentelemetry.sdk._logs import LoggerProvider, LoggingHandler
from opentelemetry.sdk._logs.export import BatchLogRecordProcessor
from azure.monitor.opentelemetry.exporter import AzureMonitorLogExporter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from azure.monitor.opentelemetry.exporter import AzureMonitorTraceExporter
from opentelemetry.instrumentation.requests import RequestsInstrumentor
from opentelemetry.instrumentation.flask import FlaskInstrumentor
import logging

def setup_metrics(connection_string):
    """
    Sets up metrics collection and exporting to Azure Monitor.

    Args:
        connection_string (str): Connection string for Azure Monitor.

    Returns:
        tuple: A tuple containing the counter, gauge, and histogram metric instruments.
    """
    # Create an exporter for Azure Monitor
    exporter = AzureMonitorMetricExporter(connection_string=connection_string)
    # Create a reader that periodically exports metrics
    reader = PeriodicExportingMetricReader(exporter, export_interval_millis=5000)
    # Set the meter provider with the reader
    metrics.set_meter_provider(MeterProvider(metric_readers=[reader]))
    meter = metrics.get_meter_provider().get_meter("Meter A")

    # Create metric instruments
    counter = meter.create_up_down_counter("test_counter")
    gauge = meter.create_gauge("test_gauge")
    histogram = meter.create_histogram("test_histogram")

    return counter, gauge, histogram

def setup_logging(connection_string):
    """
    Sets up logging and exporting logs to Azure Monitor.

    Args:
        connection_string (str): Connection string for Azure Monitor.

    Returns:
        Logger: Configured logger instance.
    """
    # Create a logger provider and set it
    logger_provider = LoggerProvider()
    set_logger_provider(logger_provider)

    # Create an exporter for Azure Monitor
    exporter = AzureMonitorLogExporter(connection_string=connection_string)
    # Add a log record processor to the logger provider
    logger_provider.add_log_record_processor(BatchLogRecordProcessor(exporter))

    # Create a logging handler and configure the logger
    handler = LoggingHandler()
    logger = logging.getLogger(__name__)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    # Add a console handler for logging to the console
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    return logger

def setup_tracing(connection_string, app):
    """
    Sets up tracing and exporting traces to Azure Monitor.

    Args:
        connection_string (str): Connection string for Azure Monitor.
        app (Flask): Flask application instance.

    Returns:
        Tracer: Configured tracer instance.
    """
    # Create a tracer provider and set it
    tracer_provider = TracerProvider()
    trace.set_tracer_provider(tracer_provider)
    tracer = trace.get_tracer(__name__)

    # Create an exporter for Azure Monitor
    exporter = AzureMonitorTraceExporter(connection_string=connection_string)
    # Add a span processor to the tracer provider
    span_processor = BatchSpanProcessor(exporter)
    trace.get_tracer_provider().add_span_processor(span_processor)

    # Instrument Flask and Requests for tracing
    FlaskInstrumentor().instrument_app(app)
    RequestsInstrumentor().instrument()

    return tracer