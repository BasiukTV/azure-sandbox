# App Insights OpenTelemetry Flask Test App

## Dependencies
1. Azure Application Insights resource
2. Run ```pip install -r requirements.txt``` (see below)

## Authorization
You have to set APP_INSIGHTS_CONN_STR environmental variable with the value of your Application Insights Connection String. Like (on Unix) ```export APP_INSIGHTS_CONN_STR='<your_app_insights_connection_string>'```

## Python Environment Setup

### Creating a Virtual Environment
1. Create a virtual environment:
    ```sh
    python3 -m venv venv
    ```
2. Activate the virtual environment:
    - On Unix or MacOS:
        ```sh
        source venv/bin/activate
        ```
    - On Windows:
        ```sh
        .\venv\Scripts\activate
        ```

### Installing Dependencies
3. Install the required dependencies:
    ```sh
    pip install -r requirements.txt
    ```

### Deactivating the Virtual Environment
4. When done, deactivate the virtual environment:
    ```sh
    deactivate
    ```

### Usage

#### How to run
```python3 app-insights-opentelemetry-tester.py```

#### Hot to use
1. By default the application will start on http://localhost:80/
2. Open the index page, and navigate to to other pages to manipulate metrics, logs, and traces exported to App Insights
3. Go to your App Insights instance, and inspect the custom metrics, logs, and traces
