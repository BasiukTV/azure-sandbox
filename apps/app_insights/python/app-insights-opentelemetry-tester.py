"""
    Application Insights OpenTelemetry Exporter Flask Tester.

    Provides a simple Flask App to test exporting OpenTelemetry metrics, logs,
    and traces into Azure Application Insights.

    The code was largely inspired by the below here:
    https://learn.microsoft.com/en-us/python/api/overview/azure/monitor-opentelemetry-exporter-readme
"""

import os, sys, random, socket
from flask import Flask, send_from_directory, url_for, render_template
from requests import get
from observability_config import setup_metrics, setup_logging, setup_tracing
from opentelemetry import trace

app = Flask(__name__)
hostname = socket.gethostname()

# Retrieve the Application Insights connection string from environment variables
APP_INSIGHTS_CONN_STR = os.environ.get("APP_INSIGHTS_CONN_STR")
if not APP_INSIGHTS_CONN_STR:
    print("ERROR! APP_INSIGHTS_CONN_STR required environmental variable was not set!")
    sys.exit(1)

# Setup configurations for metrics, logging, and tracing
counter, gauge, histogram = setup_metrics(APP_INSIGHTS_CONN_STR)
logger = setup_logging(APP_INSIGHTS_CONN_STR)
tracer = setup_tracing(APP_INSIGHTS_CONN_STR, app)

### Index Page ###

@app.route('/')
def index():
    """
    Renders the index page.
    """
    return render_template(
        "index.html",
        favicon_path = url_for('static', filename='favicon.ico'),
        hostname = hostname)

### Metrics Endpoints ###

# Supported operations for each metric type
supported_metrics_operations = {
    "counter" : {
        "add" : { "desc" : "Counter value was incremented!" },
        "sub" : { "desc" : "Counter value was decremented!" }
    },
    "gauge" : {
        "random" : { "desc" : "Gauge value was set to: " }
    },
    "histogram" : {
        "random" : { "desc" : "Histogram was updated with a value of: " }
    }
}

# Descriptions for each metric type
metrics_types_hints = {
    "counter" : "Each host continuously maintains its own counter value that can be dynamically incremented or decremented.",
    "gauge" : "Each host continuously maintains its own gauge value that can be dynamically set to any value.",
    "histogram": "Each host continuously maintains its own histogram of values. A new value can be added to the histogram at any point."
}

@app.route('/metrics/<type>/<operation>')
def add_metric(type, operation):
    """
    Endpoint to add a metric.

    Args:
        type (str): The type of metric (counter, gauge, histogram).
        operation (str): The operation to perform on the metric (add, sub, random).

    Returns:
        Response: Rendered template with the result of the metric operation.
    """
    # Input validation
    if type not in supported_metrics_operations:
        return render_template(
            "error.html",
            favicon_path = url_for('static', filename='favicon.ico'),
            hostname = hostname,
            error_message = f"Sorry, metric type {type} is not supported. Only supported metric types are { list(supported_metrics_operations.keys())}"), 400

    if operation not in supported_metrics_operations[type]:
        return render_template(
            "error.html",
            favicon_path = url_for('static', filename='favicon.ico'),
            hostname = hostname,
            error_message = f"Sorry, operation {operation} is not supported for metric type {type}. Only supported operations for {type} are { list(supported_metrics_operations[type].keys()) }"), 400

    desc, hint, r = None, None, None
    if operation == "random":
        r = random.randint(0, 100)

    if type == "counter":
        desc = supported_metrics_operations["counter"][operation]["desc"]
        hint = metrics_types_hints["counter"]

        counter.add(1 if operation == "add" else -1)

    elif type == "gauge":
        desc = supported_metrics_operations["gauge"][operation]["desc"]
        hint = metrics_types_hints["gauge"]

        gauge.set(r)

    elif type == "histogram":
        desc = supported_metrics_operations["histogram"][operation]["desc"]
        hint = metrics_types_hints["histogram"]

        histogram.record(r)

    return render_template(
        "metrics.html",
        type = type,
        operation = operation,
        favicon_path = url_for('static', filename='favicon.ico'),
        hostname = hostname,
        desc = desc,
        hint = hint,
        r = r)

### Logs Endpoints ###

# Supported log levels
supported_log_levels = set(["DEBUG", "INFO", "WARN", "ERROR"])

@app.route('/logs/<level>')
def add_log(level):
    """
    Endpoint to add a log entry.

    Args:
        level (str): The log level (DEBUG, INFO, WARN, ERROR).

    Returns:
        Response: Rendered template with the result of the log operation.
    """
    # Input validation
    if level not in supported_log_levels:
        return render_template(
            "error.html",
            favicon_path = url_for('static', filename='favicon.ico'),
            hostname = hostname,
            error_message = f"Sorry, log level {level} is not supported. Only supported levels are {supported_log_levels}"), 400

    r = random.randint(0, 100) # Random datum to add to a log message
    log_message = f"This is a {level} log. Here's some additional data: {r}"

    # Call the appropriate logger method based on the log level
    logger_method_switch = {
        "DEBUG" : logger.debug,
        "INFO"  : logger.info,
        "WARN"  : logger.warning,
        "ERROR" : logger.error,
    }
    logger_method_switch.get(level)(log_message)

    return render_template(
        "logs.html",
        level = level,
        favicon_path = url_for('static', filename='favicon.ico'),
        hostname = hostname,
        r = r)

### Tracing Endpoints ###

# Supported tracing stages
tracing_stages = set(["start", "continue", "finish"])

@app.route('/trace/backend/<stage>')
def start_trace(stage):
    """
    Endpoint to start a trace.

    Args:
        stage (str): The stage of the trace (start, continue, finish).

    Returns:
        Response: Rendered template with the result of the trace operation.
    """
    # Input validation
    if stage not in tracing_stages:
        return render_template(
            "error.html",
            favicon_path = url_for('static', filename='favicon.ico'),
            hostname = hostname,
            error_message = f"Sorry, trace stage {stage} is not supported. Only supported levels are {tracing_stages}"), 400

    r = random.randint(0, 100) # Random datum

    if stage == "start":
        with tracer.start_as_current_span("test-span"):
            span = trace.get_current_span()
            span.add_event(name = "span-start-event", attributes = {"data": r})

            return get(url="http://localhost/trace/backend/continue").text
    elif stage == "continue":
        span = trace.get_current_span()
        span.add_event(name = "span-continue-event", attributes = {"data": r})

        return get(url="http://localhost/trace/backend/finish").text
    elif stage == "finish":
        span = trace.get_current_span()
        span.add_event(name = "span-finish-event", attributes = {"data": r})

        return render_template(
            "trace.html",
            favicon_path = url_for('static', filename='favicon.ico'),
            hostname = hostname)

@app.route('/favicon.ico')
def favicon():
    """
    Endpoint to serve the favicon.
    """
    return send_from_directory('static', 'favicon.ico', mimetype='image/vnd.microsoft.icon')

# Finally, run the app
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80, debug=True)
