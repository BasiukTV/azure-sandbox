# Azure Email Communication Services Test App

## Dependencies
1. Azure Communication Service resource (and its connections string)
2. Azure Email Communication Service resource
3. Azure Email Communication Services Domain resource (and its sender email address)
4. weatherapi.com subscription key
5. Run ```pip install -r requirements.txt```

## Authorization
You have to set the following environmental variable (Unix example below):
* ```export WEATHER_API_AUTH_KEY='<your_weather_api_auth_key>'```
* ```export EMAIL_CLIENT_CONN_STRING='<your_email_comm_srv_client_connection_string>'```

### Usage

#### How to run
```
python3 weather_api_forecast.py \
  --forecast_city FORECAST_CITY \
  --recipient_email RECIPIENT_EMAIL \
  --sender_email_address SENDER_EMAIL_ADDRESS
```

#### Expected Outputs
1. A CSV file in the current directory with some of the current day forecast details
2. Email delivered to the RECIPIENT_EMAIL with the same details
