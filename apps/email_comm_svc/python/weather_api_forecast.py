"""
  This script does the following things:
    1. Calls weatherapi.com for the hourly weather forecast in a given city
    2. Saves some detail of the hourly forecast into a local CSV file
    3. Sends an email with the same details to a given recipient

  Usage Example:
    1. For help with the input parameters run:
          python3 weather_api_forecast.py -h
    2. Actual Usage example (Bash):
          python3 weather_api_forecast.py \
            --forecast_city FORECAST_CITY \
            --recipient_email RECIPIENT_EMAIL \
            --sender_email_address SENDER_EMAIL_ADDRESS

  Inputs:
    1. WEATHER_API_AUTH_KEY env var - weatherapi.com API authentication key
    2. EMAIL_CLIENT_CONN_STRING env var - Azure Email Communication Client connection string
    3. recipient_email cli arg - Recipient email address
    4. sender_email_address cli arg - Sender email address
    5. forecast_city cli arg - City name for the weather forecast

  Outputs:
    1. Local CSV file with the forecast details
    2. Email with the forecast details sent to a given recipient

  Dependencies:
    1. weatherapi.com account and the corresponding authorization key
    2. Azure Email Communication Client resource with with a validated sender domain
    3. azure-communication-email package installed with pip
"""

### Imports ###

import os # To read in the environmental variables
import argparse # To parse the input variables
import http.client # For making calls to external APIs
import sys # To halt the execution of the script with an error code if needed
import json # For parsing JSON API response
import urllib.parse # For URL encoding of API request parameters
import csv # For saving weather forecasts into CSV files

# Below needs 'pip install azure-communication-email'
from azure.communication.email import EmailClient

### Constants ###

WEATHER_API_URL = "api.weatherapi.com"

# We will call current day weather forecast endpoint
FORECAST_API_ENDPOINT = "/v1/forecast.json"

### Inputs Parsing and Validation ###

# Pick up required credentials from the environmental variables
weather_api_auth_key = os.environ.get("WEATHER_API_AUTH_KEY")
if not weather_api_auth_key:
    print("ERROR! WEATHER_API_AUTH_KEY required environmental variable was not set!")
    sys.exit(1)

email_client_conn_string = os.environ.get("EMAIL_CLIENT_CONN_STRING")
if not email_client_conn_string:
    print("ERROR! EMAIL_CLIENT_CONN_STRING required environmental variable was not set!")
    sys.exit(1)

# We use argparse library to extract the inputs into some variables
parser = argparse.ArgumentParser(description='Weather Forecast and Email Notification Script.')

parser.add_argument('--forecast_city', type=str, required=True, help='City for weather forecast.')
parser.add_argument('--recipient_email', type=str, required=True, help='Recipient email address.')
parser.add_argument('--sender_email_address', type=str, required=True, help='Sender email address.')

args = parser.parse_args()

# Extract the arguments into variables
forecast_city = args.forecast_city
recipient_email = args.recipient_email
sender_email_address = args.sender_email_address

### Calling WeatherAPI.com ###

json_data = None # Returned JSON data container

try:
    # Create a connection client to Weather API
    conn = http.client.HTTPSConnection(WEATHER_API_URL)

    # We prepare the request parameters here, see the documentation - https://www.weatherapi.com/api-explorer.aspx#forecast
    params = {
        'key': weather_api_auth_key, # Authentication key
        'q': forecast_city, # The city of interest
        'days': 1, # Forecast for one day ahead
        'aqi': 'no', # Don't require air quality data
        'alerts': 'no' # Don't require weather alerts
    }

    # Encode the parameters into a URL-safe string
    query_string = urllib.parse.urlencode(params)

    # Make a GET request object to the endpoint with the parameters
    conn.request("GET", f"{FORECAST_API_ENDPOINT}?{query_string}")

    # Get the response
    response = conn.getresponse()
    data = response.read() # Response body in JSON format

    # Check if the request was successful
    if response.status == 200:
        # Parse the JSON response into a python dictionary
        json_data = json.loads(data)
    else:
        # Some debugging steps if something goes wrong
        print(f'Error: {response.status}')
        print(f'Response Reason: {response.reason}')
        print(f'Response Body: {response.read()}')
        sys.exit(1)  # Exit the script with a non-zero status code

except (http.client.HTTPException, OSError, ValueError) as e:
    print(f"An error occurred: {e}")
    sys.exit(1)  # Exit the script with a non-zero status code

finally:
    # Close the connection
    conn.close()

# Next we parse the data to extract hourly forecast for the location of interest
# Consult Weather API explorer to see the shape of the response data
# Extracting {'forecast': { 'forecastday': [{ 'hour': [ X ] }], ... }, ...}
hourly_forecast = json_data['forecast']['forecastday'][0]['hour'] # '0' is for forecast of the current day

### Saving the forecast data into a CSV file. ###

# We will only keep time, expected temperature, wind, humidity, and weather icon from the hourly forecast

# We initialize the list with CSV header row
filtered_forecast = [('Time', 'Expected Temperature (F)', 'Expected Wind (mph)', 'Wind Direction', 'Humidity (%)', 'Weather Icon')]

# For each hour in forecast, add another tuple to our list
for hour_data in hourly_forecast:
    filtered_forecast.append((
        hour_data['time'].split(' ')[1], # Only keep the hour
        hour_data['temp_f'], # Expected temperature in Fahrenheit
        hour_data['wind_mph'], # Expected wind speed in mph
        hour_data['wind_dir'], # Expected wind direction
        hour_data['humidity'], # Expected humidity
        hour_data['condition']['icon'][2:] # Weather icon URL (dropping firs two '//' characters)
    ))

# We now want to save the forecast in a CSV file for historical data analysis

# CSV file name will contain the location and current date at location
location = json_data['location']['name']
date = json_data['location']['localtime'].split(' ')[0] # Turns '2024-08-19 00:00' into '2024-08-19'
csv_file_name = f'{location.replace(' ', '')}-{date}.csv' # Example filename 'NewYork-2024-08-19.csv'

# Write the filtered_forecast to the CSV file
with open(csv_file_name, mode='w', newline='') as file:
    writer = csv.writer(file) # Create a CSV writer

    # Write each tuple as a row in the CSV
    writer.writerows(filtered_forecast)

print(f'Forecast data has been written to {csv_file_name}')

# Now we want to send out an email with the forecast data
try:
    # Create the EmailClient object that we will use to send Email messages.
    email_client = EmailClient.from_connection_string(email_client_conn_string)

    # Prepare the message, with subject, html content, sender and recipient
    message = {
        "content": {
            "subject": f"Weather Forecast for {location} on {date}",
            "html":
              f"""<html>
                    <head>
                      <style>
                        table {'{ width: 100%; border-collapse: collapse; }'}
                        th, td {'{ padding: 10px; border: 1px solid #ddd; }'}
                        th {'{ background-color: #f4f4f4; }'}
                      </style>
                    </head>
                    <body>
                      <h1>Weather Forecast for {location} on {date}</h1>
                      <table border='1' style='width: 100%; border-collapse: collapse;'>
                        <tr><th>Time</th><th>Temperature</th><th>Wind</th><th>Humidity</th><th>Icon</th></tr>
                            """ +
                                # Using python list comprehension to prepare 24 table rows
                                "".join([f"""
                                    <tr>
                                      <td id='time'> {hour[0]}</td>
                                      <td id='temp'> {hour[1]} F</td>
                                      <td id='wind'> {hour[2]} mph {hour[3]}</td>
                                      <td id='humid'>{hour[4]} %</td>
                                      <td id='icon'><img src='{hour[5]}'></td>
                                    </tr>""" for hour in filtered_forecast[1:]])
                            + """
                      </table>
                    </body>
                  </html>"""
        },
        "recipients": {
            "to": [
                {
                    "address": recipient_email,
                    "displayName": recipient_email
                }
            ]
        },
        "senderAddress": sender_email_address
    }

    # Actually send out the email
    poller = email_client.begin_send(message)
    print("Email Send Result: ")
    print(poller.result())

# If any exception occurred simply print it out at this point
except Exception as ex:
    print('Exception occurred, while trying to send out the email:')
    print(ex)
    sys.exit(1)
