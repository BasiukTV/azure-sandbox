# Sample Stream Analytics Job Apps

## Analytics with Event Hub Input and Azure SQL DB Output

### Steps

1. Run the [Terraform](../../../automation/iac/stream_analytics/terraform/) script to get the infrastructure.
2. Query the Terraform output to get the Event Hub name and connection string and Azure SQL server, database, and admin credentials.
3. Connect to the SQL database and create the SQL table using this [SQL script](../../../data/stream_analytics/sql/out_mssql_table.sql)
4. Start the Stream Analytics Job.
5. Follow the instructions on how to run [Event Hub Event Producer](../../../apps/eventhub/python/README.md) to produce some input data.
6. Observer the analytics result arrive to the SQL table created above.

### Cleanup
* Follow the Terraform cleanup instructions [here](../../../automation/iac/stream_analytics/terraform/README.md)
