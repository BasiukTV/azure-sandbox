﻿// NOTE: The code here was adapted from https://learn.microsoft.com/en-us/azure/service-bus-messaging/service-bus-dotnet-how-to-use-topics-subscriptions
// TODO: Replace the <NAMESPACE-CONNECTION-STRING>, <TOPIC-NAME> and <SUBSCRIPTION-NAME> placeholders.

using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;

// The client that owns the connection and can be used to create senders and receivers
await using var client = new ServiceBusClient("<NAMESPACE-CONNECTION-STRING>");

// The processor that reads and processes messages from the subscription
await using var processor = client.CreateProcessor("<TOPIC-NAME>", "<SUBSCRIPTION-NAME>", new ServiceBusProcessorOptions());

// Handle received messages.
async Task MessageHandler(ProcessMessageEventArgs args)
{
  string body = args.Message.Body.ToString();
  Console.WriteLine($"Received: {body} from subscription.");

  // Complete the message. Messages is deleted from the subscription.
  await args.CompleteMessageAsync(args.Message);
}

// Handle any errors when receiving messages.
Task ErrorHandler(ProcessErrorEventArgs args)
{
  Console.WriteLine(args.Exception.ToString());
  return Task.CompletedTask;
}

// Add handler to process messages.
processor.ProcessMessageAsync += MessageHandler;

// Add handler to process any errors.
processor.ProcessErrorAsync += ErrorHandler;

// Start processing messages.
await processor.StartProcessingAsync();

Console.WriteLine("Wait for a minute and then press any key to end the processing.");
Console.ReadKey();

// Stop processing messages.
Console.WriteLine("\nStopping the receiver...");
await processor.StopProcessingAsync();
Console.WriteLine("Stopped receiving messages.");

await processor.DisposeAsync();
await client.DisposeAsync();
