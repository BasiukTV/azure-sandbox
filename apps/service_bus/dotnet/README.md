# Sample .Net code for sending and receiving message over Azure Service Bus

## Dependencies
1. [.Net Framework SDK](https://dotnet.microsoft.com/en-us/download)
2. Provisioned Azure Service Bus. Can use [this](../../../automation/iac/service_bus/bicep/) BICEP configuration.

## How to Run
1. Navigate into the subdirectory of the desired app
2. Replace service bus connection strings, namespaces, queues/topics (and maybe subscriptions) placeholders in the ```Program.cs``` file.
3. Run the below:
```
dotnet build
dotnet run
```
