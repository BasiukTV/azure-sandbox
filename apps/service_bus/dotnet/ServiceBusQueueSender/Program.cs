﻿// NOTE: This code was adapted from https://learn.microsoft.com/en-us/azure/service-bus-messaging/service-bus-dotnet-get-started-with-queues
// TODO: Replace the <NAMESPACE-CONNECTION-STRING> and <QUEUE-NAME> placeholders below

using Azure.Messaging.ServiceBus;

// Set the transport type to AmqpWebSockets so that the ServiceBusClient uses the port 443.
// If you use the default AmqpTcp, you will need to make sure that the ports 5671 and 5672 are open
var clientOptions = new ServiceBusClientOptions()
{
  TransportType = ServiceBusTransportType.AmqpWebSockets
};

// The client that owns the connection and can be used to create senders and receivers
await using var client = new ServiceBusClient("<NAMESPACE-CONNECTION-STRING>", clientOptions);

// The sender used to publish messages to the queue
await using var sender = client.CreateSender("<QUEUE-NAME>");

// Number of messages to be sent to the queue
const int numOfMessages = 3;

// Create a batch of messages and populate it
using var messageBatch = await sender.CreateMessageBatchAsync();
for (int i = 1; i <= numOfMessages; i++)
{
  // try adding a message to the batch
  messageBatch.TryAddMessage(new ServiceBusMessage($"Message {i}"));
}

// Use the producer client to send the batch of messages to the Service Bus queue
await sender.SendMessagesAsync(messageBatch);
Console.WriteLine($"A batch of {numOfMessages} messages has been published to the queue.");

Console.WriteLine("Press any key to end the application...");
Console.ReadKey();
