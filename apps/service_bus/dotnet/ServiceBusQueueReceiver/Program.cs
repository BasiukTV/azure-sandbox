﻿// NOTE: This code was adapted from https://learn.microsoft.com/en-us/azure/service-bus-messaging/service-bus-dotnet-get-started-with-queues
// TODO: Replace the <NAMESPACE-CONNECTION-STRING> and <QUEUE-NAME> placeholders below

using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;

// Set the transport type to AmqpWebSockets so that the ServiceBusClient uses port 443.
// If you use the default AmqpTcp, make sure that ports 5671 and 5672 are open.
var clientOptions = new ServiceBusClientOptions()
{
  TransportType = ServiceBusTransportType.AmqpWebSockets
};

// The client that owns the connection and can be used to create senders and receivers
// await using var client = new ServiceBusClient("<NAMESPACE-CONNECTION-STRING>", clientOptions);
await using var client = new ServiceBusClient("Endpoint=sb://sand-svcbus-tst-eastus-sbn.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=YRI60emFjo0re2Wfl+w71LkPmVi2FwYQa+ASbPST1K0=", clientOptions);

// The processor that reads and processes messages from the queue
// await using var processor = client.CreateProcessor("<QUEUE-NAME>", new ServiceBusProcessorOptions());
await using var processor = client.CreateProcessor("sand-svcbus-tst-eastus-sbq", new ServiceBusProcessorOptions());

// Handle received Service Bus messages
async Task MessageHandler(ProcessMessageEventArgs args)
{
  string body = args.Message.Body.ToString();
  Console.WriteLine($"Received: {body}");

  // Complete processing the message. Message is deleted from the queue.
  await args.CompleteMessageAsync(args.Message);
}

// Add the handler to process messages
processor.ProcessMessageAsync += MessageHandler;

// Handle any errors when receiving messages
Task ErrorHandler(ProcessErrorEventArgs args)
{
  Console.WriteLine(args.Exception.ToString());
  return Task.CompletedTask;
}

// Add handler to process any errors
processor.ProcessErrorAsync += ErrorHandler;

// Start processing of Messages
await processor.StartProcessingAsync();

Console.WriteLine("Wait for a minute and then press any key to end the processing");
Console.ReadKey();

Console.WriteLine("\nStopping the receiver...");
await processor.StopProcessingAsync();
Console.WriteLine("Stopped receiving messages. Done.");
