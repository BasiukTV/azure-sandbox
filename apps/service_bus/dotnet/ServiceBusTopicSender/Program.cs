﻿// NOTE: The code here was adapted from https://learn.microsoft.com/en-us/azure/service-bus-messaging/service-bus-dotnet-how-to-use-topics-subscriptions
// TODO: Replace the "<NAMESPACE-CONNECTION-STRING>" and "<TOPIC-NAME>" placeholders.

using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;

// The client that owns the connection and can be used to create senders and receivers
await using var client = new ServiceBusClient("<NAMESPACE-CONNECTION-STRING>");

// The sender used to publish messages to the topic
await using var sender = client.CreateSender("<TOPIC-NAME>");

// Number of messages to be sent to the topic
const int numOfMessages = 3;

// Create a batch of messages
using ServiceBusMessageBatch messageBatch = await sender.CreateMessageBatchAsync();

for (int i = 1; i <= numOfMessages; i++)
{
  // try adding a message to the batch
  messageBatch.TryAddMessage(new ServiceBusMessage($"Message {i}"));
}

await sender.SendMessagesAsync(messageBatch);
Console.WriteLine($"A batch of {numOfMessages} messages has been published to the topic.");

Console.WriteLine("Press any key to end the application...");
Console.ReadKey();
