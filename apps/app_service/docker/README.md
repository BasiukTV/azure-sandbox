# App Service Docker App Deployments

## Prerequisites

## Deployment Scripts
1. Azure App Service (can use this [Terraform Configuration](../../../automation/iac/app_service/terraform/))
2. Docker image with an application to run.
3. (Azure CLI)[https://learn.microsoft.com/en-us/cli/azure/] / (Azure Powershell)[https://learn.microsoft.com/en-us/powershell/azure/install-azure-powershell]

### Azure CLI
1. Run ```az login```
1. Modify and run below script (for public registries - omit user nad password)
```
az webapp create \
    --name <your-app-service-name> \
    --plan <your-app-service-plan-name> \
    --resource-group <your-resource-group-name> \
    --deployment-container-image-name <your-public-doker-image:tag> \
    --docker-registry-server-user <your-docker-repository-user> \
    --docker-registry-server-password <your-docker-repository-password>
```

### Azure Powershell
1. Run ```Connect-AzAccount```
1. Modify and run below script (for public registries give any non empty password when prompted)
```
New-AzWebApp -ResourceGroupName "YourResourceGroupName" `
  -Name "YourAppServiceName" `
  -ContainerImageName "YourDockerImage" `
  -ContainerRegistryUrl "YourDockerImageRegistry" `
  -ContainerRegistryUser "YourDockerRegistryUsername"
```