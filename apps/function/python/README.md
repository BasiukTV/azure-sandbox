# Sample Python function code for the Azure Function App

## Dependencies
1. [az cli](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli#install)
2. [Azure Function Core Tools](https://learn.microsoft.com/en-us/azure/azure-functions/create-first-function-cli-python?tabs=windows%2Cbash%2Cazure-cli%2Cbrowser#install-the-azure-functions-core-tools)
3. Provisioned Azure Function App. Can use [this](../../../automation/iac/function/terraform/) Terraform configuration.

## Steps to Deploy the Function
Run in the current directory:
```
az login
func azure functionapp publish <your-function-app-name>
```

## How to Test

* Note, If deployed the function from this directory <your-function-name> is test_http_trigger

### Getting the credentials
1. To get the function URL: ```az functionapp show --resource-group <your-rg-name> --name <your-function-app-name> --query defaultHostName --output tsv```
2. To get the function key: ```az functionapp function keys list --resource-group <your-rg-name> --name <your-function-app-name> --function-name <your-function-name> --query default --output tsv```

### Calling the Function
Call the following URL from your browser:
```https://<your-function-url>/api/<your-function-name>?code=<your-function-key>&name=<your-name>```
