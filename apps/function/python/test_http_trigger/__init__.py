import logging, os
from datetime import datetime

from azure.functions import HttpRequest, HttpResponse


def main(req: HttpRequest) -> HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    name = req.params.get('name')
    if not name:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('name')

    # Time of the previous function call
    prev_call = os.environ.get('PREV_CALL')
    prev_call_msg = f"This function was previously called on: {prev_call if prev_call else 'NEVER'}"

    # Set/Update previous function call time
    os.environ['PREV_CALL'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    if name:
        return HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully. {prev_call_msg}")
    else:
        return HttpResponse(
            f"This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response. {prev_call_msg}",
            status_code=200
        )
