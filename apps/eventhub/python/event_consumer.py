from azure.eventhub import EventHubConsumerClient, EventData

import json, jsonschema
from datetime import datetime, timedelta

# Container for the message schema object used for the validation of the received events
message_schema = None

# Function asynchronously executed whenever new event is consumed from partition, or max_wait_time reached.
def process_event(context, data):
    t = datetime.now() # Current time
    p = context.partition_id # Partition of the event

    # If we got here due to max_wait_time reached, report no new events message
    if not data:
        print(f"{t}: Partition {p}: No (new) messages now.")
    else:
        print(f"{t}: Partition {p}: New event received. Data: {data}")

        # Now, validate the content of the new event body
        try:
          jsonschema.validate(json.loads(data.body_as_str()), message_schema)
          print(f"{t}: Message with seqnum {data.sequence_number} in partition {p} is successfully validated.")
        except jsonschema.ValidationError as e:
          print(f"{t}: Received message failed the schema validation: {e}")

# Only run when executed directly
if __name__ == "__main__":

    # Build out an argument parser to take input parameters from the user
    import argparse
    parser = argparse.ArgumentParser(
        "This will consume the Event Hub events starting from a given point back in time, and will then wait for the new ones forever... Every event body will be validated against the expected schema.")
    parser.add_argument("-s", "--starting-from", type = int, default = 60,
        help = "Starting point back in time (seconds) to consume events from.")
    parser.add_argument("-w", "--wait-time", type = int, default = 5000,
        help = "Time to wait (ms) for the new events to arrive for the current partition until reporting no new messages.")
    args = parser.parse_args()

    # Prepare the message schema to validate the generated message contents
    with open("../../../data/eventhub/schema/test_schema.json") as schema_file:
        message_schema = json.load(schema_file)

    # Read in other app configuration from file
    app_config = None
    with open("app_config.json") as app_config_file:
        app_config = json.load(app_config_file)

    ehub_name = app_config["ehub_name"]
    ehub_connection_string =  app_config["ehub_connection_string"]

    # Prepare the Event Hub consumer client
    with EventHubConsumerClient.from_connection_string(
        ehub_connection_string, eventhub_name = ehub_name, consumer_group = "$Default") as consumer :

        # Configure the event event handler, and start waiting for the new events forever...
        consumer.receive(
            on_event = process_event,
            starting_position = datetime.now() - timedelta(seconds=args.starting_from),
            max_wait_time = args.wait_time / 1000)
