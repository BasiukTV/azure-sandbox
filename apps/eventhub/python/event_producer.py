from azure.eventhub import EventHubProducerClient, EventData

import json, jsonschema, random, time

# Generates a randomized message that adheres to the expected schema
def generate_message():

    # Prepare the image skeleton
    message = json.loads('''{
        "booleanField":true,
        "numberField":0,
        "stringField":"",
        "nullField":null,
        "arrayField":[],
        "objectField":null
    }''')

    # Populate the message fields with some randomized data
    message['booleanField'] = random.randint(1, 2) % 2 == 0
    message['numberField'] = random.randint(1, 1000)
    message['stringField'] = chr(64 + random.randint(1, 26)) * random.randint(1, 100)
    message['arrayField'] = list(range(random.randint(1, 100)))
    message['objectField'] = {
        chr(64 + random.randint(1, 26)) * random.randint(1, 10) :
        chr(64 + random.randint(1, 26)) * random.randint(1, 10)}

    return message

# Only run when executed directly
if __name__ == "__main__":

    # Build out an argument parser to take input parameters from the user
    import argparse
    parser = argparse.ArgumentParser(
        "This will be sending roughly one event per second to Azure Event Hub with given duration, frequency, and length.")
    parser.add_argument("-t", "--total-duration", type = int, default = 60,
        help = "Total duration of event producer run (seconds).")
    parser.add_argument("-f", "--frequency", type = int, default = 10,
        help = "Event producer will start producing events every *value* seconds.")
    parser.add_argument("-l", "--length", type = int, default = 5,
        help = "When event producer starts producing events, it will do so for *value* seconds.")
    args = parser.parse_args()

    # Read in other app configuration from file
    app_config = None
    with open("app_config.json") as app_config_file:
        app_config = json.load(app_config_file)

    ehub_name = app_config["ehub_name"]
    ehub_connection_string =  app_config["ehub_connection_string"]

    # Prepare the Event Hub client
    producer = EventHubProducerClient.from_connection_string(ehub_connection_string, eventhub_name = ehub_name)

    # Prepare the message schema to validate the generated message contents
    message_schema = None
    with open("../../../data/eventhub/schema/test_schema.json") as schema_file:
        message_schema = json.load(schema_file)

    # Keep a record of the start and current time, run the generator for requested duration
    start_time = time.time()
    curr_time = time.time()
    while curr_time - start_time < args.total_duration :
        print(f"Time: {int(curr_time - start_time)}s ", end='')

        # If current time lies within message generation window
        if (int(curr_time - start_time) % args.frequency < args.length):
            print("Sending an event... ", end='')

            # Generate and validate the message
            message = generate_message()
            jsonschema.validate(message, message_schema)

            # Prepare the event batch (with single event) to send
            event_data_batch = producer.create_batch(
                partition_key = message["stringField"])  # Set the partition key to be the random string
            event_data = EventData(json.dumps(message))
            event_data_batch.add(event_data)

            # Finally, send out the event
            producer.send_batch(event_data_batch)
            print("DONE")
        else:
            print("Waiting...")

        time.sleep(1) # Wait for a second
        curr_time = time.time() # Update the current time
