# Sample Event Hub Apps

## Dependencies
```pip install -r requirements.txt```

## Authorization
1. Make a copy of [app_config.template.json](./app_config.template.json) file and rename it into **app_config.json**
2. Update **app_config.json** to include the target event hub and its primary connection string
    - If the Event Hub was provisioned with [this](../../../automation/iac/eventhub/terraform/) Terraform configuration, you can run ```terraform output ehub_name``` and ```terraform output ehub_connection_string``` to retrieve the above values.

## Event Producer App

### Usage

#### How to run
```python3 event_producer.py```

#### Options
```
usage: This will be sending roughly one event per second to Azure Event Hub with given duration, frequency, and length.
       [-h] [-t TOTAL_DURATION] [-f FREQUENCY] [-l LENGTH]

options:
  -h, --help            show this help message and exit
  -t TOTAL_DURATION, --total-duration TOTAL_DURATION
                        Total duration of event producer run (seconds).
  -f FREQUENCY, --frequency FREQUENCY
                        Event producer will start producing events every *value* seconds.
  -l LENGTH, --length LENGTH
                        When event producer starts producing events, it will do so for *value* seconds.
```

## Event Consumer App

### Usage

#### How to run
```python3 event_consumer.py```

#### Options
```
usage: This will consume the Event Hub events starting from a given point back in time, and will then wait for the new ones forever... Every event body will be validated against the expected schema.
       [-h] [-s STARTING_FROM] [-w WAIT_TIME]

options:
  -h, --help            show this help message and exit
  -s STARTING_FROM, --starting-from STARTING_FROM
                        Starting point back in time (seconds) to consume events from.
  -w WAIT_TIME, --wait-time WAIT_TIME
                        Time to wait (ms) for the new events to arrive for the current partition until
                        reporting no new messages.
```