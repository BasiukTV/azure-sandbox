const express = require('express');
const os = require('os');

// Azure Application Insights
const appInsights = require('applicationinsights');

const port = process.env.PORT || 3000;
const app = express();

// 1) Set up Azure App Insights (replace with your real key or connection string)
appInsights.setup('<YOUR_APP_INSIGHTS_CONNECTION_STRING>').start();
const client = appInsights.defaultClient;

// 2) Health Endpoint
// Used by Azure load balancers or probes to ensure this instance is healthy.
app.get('/health', (req, res) => {
  res.status(200).send('Healthy');
});

// 3) Status Endpoint
// Simulates CPU & memory usage, returns some info about the machine.
app.get('/status', (req, res) => {
  simulateCpuLoad();
  simulateMemoryLoad();

  const info = {
    message: 'API is running',
    machine: os.hostname(),
    timestamp: new Date().toISOString(),
  };
  
  // Track event in Application Insights
  client.trackEvent({ name: 'StatusEndpointCalled', properties: info });

  res.json(info);
});

// CPU Load Simulation
function simulateCpuLoad() {
  const start = Date.now();
  // Busy loop for ~100ms
  while (Date.now() - start < 100) {
    // do nothing
  }
}

// Memory Load Simulation
function simulateMemoryLoad() {
  let memoryArray = [];

  // This will consume some memory (1 million elements) on each call.
  memoryArray.push(new Array(1_000_000).fill('*'));
}

// 4) Start the server
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
