# Status App for Azure Load Balancer Testing

## Purpose

* Start a simple Web API application to be used for Azure Load Balancers testing.
* App exposes ```/status``` HTTP endpoint:
  *  Endpoint puts light CPU and Memory load obt the server.
  *  Endpoints returns JSON payload with timestamp and hostname.
  *  Endpoint send the same payload to Azure Application Insights as an event.
* App also exposes ```/health``` enpoint for health probes.

## Usage

### Testing Locally

Run:
1. ```sudo apt update```
2. ```sudo apt install nodejs```
3. ```sudo apt install npm```
4. ```npm init -y```
5. ```npm install express applicationinsights```
6.  ```
    npm install @opentelemetry/api @opentelemetry/sdk-node \
        @opentelemetry/auto-instrumentations-node \
        @opentelemetry/exporter-trace-otlp-http \
        @opentelemetry/exporter-metrics-otlp-http
    ```
7. Replace ```<YOUR_APP_INSIGHTS_CONNECTION_STRING>``` with your value.
8. ```node server.js```

### Running on a remote server (Ubuntu)

1. Replace ```<YOUR_APP_INSIGHTS_CONNECTION_STRING>``` in [server.js](./server.js) with your value.
2. Copy [server.js](./server.js) into a ```/home/<username>/status_app/``` folder on a remote server.
3. Run:
   1. ```sudo apt update```
   2. ```sudo apt install nodejs```
   3. ```sudo apt install npm```
   4. ```npm init -y```
   5. ```npm install express applicationinsights```
   6. ```
         npm install @opentelemetry/api @opentelemetry/sdk-node \
             @opentelemetry/auto-instrumentations-node \
             @opentelemetry/exporter-trace-otlp-http \
             @opentelemetry/exporter-metrics-otlp-http
      ```
4. Replace ```taras``` in [statusapp.service](./statusapp.service) with your username on the server.
5. Copy [statusapp.service](./statusapp.service) into a ```/etc/systemd/system/``` folder on a remote server.
6. Run:
   1. ```sudo systemctl daemon-reload```
   2. ```sudo systemctl enable statusapp.service```
   3. ```sudo systemctl start statusapp.service```
   4. ```systemctl status statusapp.service```
7. Capture the image of the running VM using Azure Image Gallery and use it to quickly spin app additional VMs for load testing.
