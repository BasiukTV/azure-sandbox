#!/usr/bin/env python3

import argparse
import requests
import time
from concurrent.futures import ThreadPoolExecutor, as_completed

def parse_arguments():
    """
    Parse command-line arguments using argparse.
    """
    parser = argparse.ArgumentParser(description="A simple load test tool.")

    parser.add_argument("--host", type=str, default="http://localhost:80",
        help="Host (URL) to test. Default is http://localhost:80")

    parser.add_argument("--total_requests", type=int, default=1000,
        help="Total number of requests to send. Default is 1000.")

    parser.add_argument("--threads", type=int, default=3,
        help="Number of threads (concurrent workers) to use. Default is 3.")

    return parser.parse_args()

def send_request(url, request_number):
    """
    Sends a GET request to the specified URL.
    Returns True if the request succeeds (2xx), else False.
    """
    start_time = time.time()
    try:
        response = requests.get(url, timeout=5)
        latency = time.time() - start_time
        print(f"Request {request_number}: {response.status_code} - {response.text[:100]!r} (latency: {latency:.3f}s)")
        return 200 <= response.status_code < 300
    except Exception as e:
        latency = time.time() - start_time
        print(f"Request {request_number}: Error - {e} (latency: {latency:.3f}s)")
        return False

def main():
    args = parse_arguments()

    host = args.host
    total_requests = args.total_requests
    num_threads = args.threads

    print(f"Starting load test with the following parameters:")
    print(f"  Host: {host}")
    print(f"  Total requests: {total_requests}")
    print(f"  Threads: {num_threads}")

    success_count = 0
    fail_count = 0

    # Use ThreadPoolExecutor to run requests in parallel
    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        # Submit tasks
        futures = [executor.submit(send_request, host, i + 1) for i in range(total_requests)]

        # Process the results as they complete
        for future in as_completed(futures):
            if future.result():
                success_count += 1
            else:
                fail_count += 1

    # Print a summary of the run
    print("\nLoad Test Results:")
    print(f"  Successful requests: {success_count}")
    print(f"  Failed requests: {fail_count}")
    print(f"  Success rate: {success_count / total_requests * 100:.2f}%")

if __name__ == "__main__":
    main()
