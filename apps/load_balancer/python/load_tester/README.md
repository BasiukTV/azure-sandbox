# Load Testing Script

## Overview
This script sends multiple GET requests to a specified host to measure performance and reliability.

## Dependencies and Setup
- Requires Python 3.7+
- Install the dependencies:
```bash
pip3 install requests
```
- (Optional) Create and activate a virtual environment:
```bash
python3 -m venv venv
source venv/bin/activate
pip3 install requests
```

## Usage

```bash
python3 load_tester.py --host <URL> --total_requests <NUMBER> --threads <NUMBER>
```

### Arguments

- **--host**: The base URL to test. (Default: `http://localhost:80`)
- **--total_requests**: Total number of requests. (Default: `1000`)
- **--threads**: Number of concurrent threads. (Default: `3`)

## Example

```bash
python3 load_tester.py --host http://example.com --total_requests 500 --threads 5
```