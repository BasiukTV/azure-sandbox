# Desired State Configuration (DSC) for Windows Server with Ansible

## Prerequisites
1. Azure VNet with two subnets, one for Ansible server, one for Controlled Windows Server VM
2. Ansible server subnet has port 22 open for SSH
3. Windows server subnet
   1. Communication from Ansible server subnet must be allowed on port 5986, which is the case by default.
   2. Windows VM will require WinRM configuration. If this is done from outside of the VNet (not using Bastion Host or other jump server), RDP port 3389 will have to be open.
4. Ansible server VM (tested on Ubuntu 22.04) with a Public IP address
5. Windows Server VM (tested on Windows Server 2022) with only a Private IP address (unless WinRM configuration done from the outside, without using a jump server)

## Steps
1. RDP onto the Windows Server VM (use Bastion Host or other jump server).
2. Run the below commands in PowerShell as admin to configure the WinRM. Keep in mind that Ansible themselves seem skeptical about quality/security of the below script.
```
Set-ExecutionPolicy Bypass -Scope LocalMachine -Force

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$url = "https://raw.githubusercontent.com/ansible/ansible-documentation/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"

$file = "$env:temp\ConfigureRemotingForAnsible.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)

Invoke-Expression "$file -SkipNetworkProfileCheck"
```
3. SSH onto the Ubuntu Ansible server VM
4. [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html#installing-ansible-on-ubuntu)
5. Copy-paste the contents of the [inventory.ini](./inventory.ini) file into your Ansible working directory
   1. Modify Windows VM private IP address, admin login and password
6. Copy-paste the contents of the [windows_install_azure_cli.yaml](./windows_install_azure_cli.yaml) file into your Ansible working directory
7. Run ```ansible-playbook windows_install_azure_cli.yaml -i inventory.ini``` in your Ansible working directory
8. Verify that Azure CLI was installed on Windows Server VM by running ```az --version``` from PowerShell
