# Terraform IaC template for Function App deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless override is given).
2. Function App Linux Service Plan (B1 by default).
3. Storage Account to store Function App files.
4. Function App itself, configured for Python 3.10 Function deployments.

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
terraform init
terraform apply
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
