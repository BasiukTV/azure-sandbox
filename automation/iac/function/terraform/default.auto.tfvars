/*** Variables used for all resources ***/

# dept  = "sand"   # Department that owns the application.
# app   = "func"   # Name of the application.
# stage = "test"   # Application deployment stage.
# loc   = "eastus" # Application deployment location.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Variables for the Function App (and its child services) ***/

# func_app_svc_plan_SKU = "B1"
