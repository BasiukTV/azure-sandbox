output "rg_name" {
  description = "Provisioned Resource Group name."
  value       = local.rg_name
}

output "function_app_name" {
  description = "Name of the provisioned Function App."
  value       = azurerm_linux_function_app.func_app.name
}
