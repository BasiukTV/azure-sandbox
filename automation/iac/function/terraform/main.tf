terraform {
  required_providers {
    azurerm = "~> 3.100"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

resource "azurerm_service_plan" "svc_plan" {
  name                = "${local.res_name_prefix}-svc-plan"
  resource_group_name = local.rg_name
  location            = var.loc

  os_type  = "Linux"
  sku_name = var.func_app_svc_plan_SKU

  depends_on = [azurerm_resource_group.rg]
}

# We use a local Storage Account module to get the SA for the Function App files
module "sa" {
  source = "../../storage_account/terraform"

  dept  = var.dept
  app   = var.app
  stage = var.stage
  loc   = var.loc

  override_rg_name = var.override_rg_name == "" ? azurerm_resource_group.rg[0].name : local.rg_name
}

# Function App resource itself
resource "azurerm_linux_function_app" "func_app" {
  name                = "${local.res_name_prefix}-func-app"
  resource_group_name = local.rg_name
  location            = var.loc

  service_plan_id = azurerm_service_plan.svc_plan.id

  storage_account_name       = module.sa.sa_name
  storage_account_access_key = module.sa.sa_account_key

  identity {
    type = "SystemAssigned"
  }

  site_config {
    always_on = true

    application_stack {
      python_version = "3.10"
    }
  }

  # These setting were added after a Python function deployment, se we'll keep them here to avoid infra drift
  app_settings = {
    "AzureWebJobsFeatureFlags"       = "EnableWorkerIndexing"
    "BUILD_FLAGS"                    = "UseExpressBuild"
    "ENABLE_ORYX_BUILD"              = "true"
    "SCM_DO_BUILD_DURING_DEPLOYMENT" = "1"
    "XDG_CACHE_HOME"                 = "/tmp/.cache"
  }
}
