# Terraform IaC template for the Azure VNet Peering

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. VNet Peering from first VNet to the second
2. VNet Peering from second VNet to the first

## Usage

### Provision
To provision the resources, run from this directory:
```
export ARM_SUBSCRIPTION_ID=<your_azure_subscription_id>
az login
terraform init
terraform apply
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
