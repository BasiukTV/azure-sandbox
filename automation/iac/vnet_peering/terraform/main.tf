terraform {
  required_providers {
    azurerm = {
      version = ">= 4.0"
      source  = "hashicorp/azurerm"
    }
  }

  required_version = ">= 1.9"

  backend "local" {}
}

provider "azurerm" {
  features {}
}
