# Data sources for the existing VNets to be peered.
data "azurerm_virtual_network" "vnet1" {
  for_each = var.peering_config

  name                = each.value.vnet1.vnet_name
  resource_group_name = each.value.vnet1.vnet_rg_name
}

data "azurerm_virtual_network" "vnet2" {
  for_each = var.peering_config

  name                = each.value.vnet2.vnet_name
  resource_group_name = each.value.vnet2.vnet_rg_name
}

# Forward peering.
resource "azurerm_virtual_network_peering" "peering1" {
  for_each = var.peering_config

  name                      = "${data.azurerm_virtual_network.vnet1[each.key].name}_peering_to_${data.azurerm_virtual_network.vnet2[each.key].name}"
  resource_group_name       = data.azurerm_virtual_network.vnet1[each.key].resource_group_name
  virtual_network_name      = data.azurerm_virtual_network.vnet1[each.key].name
  remote_virtual_network_id = data.azurerm_virtual_network.vnet2[each.key].id
}

# Backward Peering
resource "azurerm_virtual_network_peering" "peering2" {
  for_each = var.peering_config

  name                      = "${data.azurerm_virtual_network.vnet2[each.key].name}_peering_to_${data.azurerm_virtual_network.vnet1[each.key].name}"
  resource_group_name       = data.azurerm_virtual_network.vnet2[each.key].resource_group_name
  virtual_network_name      = data.azurerm_virtual_network.vnet2[each.key].name
  remote_virtual_network_id = data.azurerm_virtual_network.vnet1[each.key].id
}
