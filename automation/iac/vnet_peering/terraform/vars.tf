variable "peering_config" {
  description = "Configuration for the desired VNet bi-directional peerings."
  type = map(object({
    vnet1 = object({
      vnet_name    = string
      vnet_rg_name = string
    })
    vnet2 = object({
      vnet_name    = string
      vnet_rg_name = string
    })
  }))

  default = {
    "peering1" : {
      vnet1 = {
        vnet_name    = "vnet-taras-vpeer-t-eu-1"
        vnet_rg_name = "rg-taras-vpeer-t-eu"
      }
      vnet2 = {
        vnet_name    = "vnet-taras-vpeer-t-eu-2"
        vnet_rg_name = "rg-taras-vpeer-t-eu"
      }
    }
  }
}
