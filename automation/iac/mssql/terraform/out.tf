output "rg_name" {
  description = "Resource Group name where the resources were provisioned."
  value       = local.rg_name
}

output "mssql_server" {
  description = "Name of the provisioned MSSQL server."
  value       = azurerm_mssql_server.mssql_server.name
}

output "mssql_db" {
  description = "Name of the provisioned MSSQL database."
  value       = azurerm_mssql_database.mssql_db.name
}

output "db_admin_login" {
  description = "DB admin login."
  value       = azurerm_mssql_server.mssql_server.administrator_login
}

output "db_admin_password" {
  description = "DB admin password."
  sensitive   = true
  value       = azurerm_mssql_server.mssql_server.administrator_login_password
}
