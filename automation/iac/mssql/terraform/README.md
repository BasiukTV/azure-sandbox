# Terraform IaC template for Azure SQL Database

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless override is given)
2. MSSQL Server
3. MSSQL Database
4. MSSQL Server Firewall to allow DB connection from the machine that just ran terraform
5. MSSQL Server Firewall to allow DB connection from other Azure Services
6. (Optionally) AdventureWorksLT sample database.

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
terraform init
terraform apply

# If need to know the MSSQL server admin password
terraform output db_admin_password
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
