terraform {
  required_providers {
    azurerm = "~> 3.96"
    random  = "~> 3.1"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

resource "random_password" "sqlserver_password" {
  length           = 20
  special          = true
  override_special = "_%@"
}

resource "azurerm_mssql_server" "mssql_server" {
  name                = "${local.rg_name}-mssql-server"
  resource_group_name = local.rg_name
  location            = var.loc

  version             = "12.0"
  minimum_tls_version = "1.2"

  administrator_login          = "${var.app}${var.stage}"
  administrator_login_password = random_password.sqlserver_password.result

  depends_on = [azurerm_resource_group.rg]
}

resource "azurerm_mssql_database" "mssql_db" {
  name = "${local.rg_name}-mssql-db"

  server_id = azurerm_mssql_server.mssql_server.id

  storage_account_type = "Local"
  sku_name             = "Basic"

  # If samle DB was requested, deploy AdventureWorksLT, otherwise deploy empty DB
  sample_name = var.sample_db ? "AdventureWorksLT" : null
}

# Retrieves the IP address of the machine running terraform
data "http" "current_ip" {
  url = "https://api.ipify.org"
}

# MSSQL Server Firewall rule to allow connection from the machine that ran terraform
resource "azurerm_mssql_firewall_rule" "allow_current_ip" {
  name = "AllowCurrentIP"

  server_id = azurerm_mssql_server.mssql_server.id

  start_ip_address = data.http.current_ip.response_body
  end_ip_address   = data.http.current_ip.response_body
}

# MSSQL Server Firewall rule to allow connection from other Azure Resources
resource "azurerm_mssql_firewall_rule" "allow_other_azure" {
  name = "AllowOtherAzureResourcesConnection"

  server_id = azurerm_mssql_server.mssql_server.id

  start_ip_address = "0.0.0.0"
  end_ip_address   = "0.0.0.0"
}
