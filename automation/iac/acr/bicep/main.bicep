/**
 * This provisions a resource group and then calls a module to provision an AKS cluster.
 */

targetScope = 'subscription'

@description('Common resource name template to be used for AKS resource deployment.')
param res_name_template string

@description('Application deployment location.')
param region string = 'eastus'

resource rg 'Microsoft.Resources/resourceGroups@2024-03-01' = {
  name: format(res_name_template, 'rg')
  location: region
}

module acr 'acr.bicep' = {
  scope: rg
  name: 'aks_module'

  params: {
    res_name_template: res_name_template
    region: region
  }
}

@description('Provisioned Resource Group name.')
output rg_name string = rg.name

@description('Provisioned ACR name.')
output acr_name string = acr.outputs.acr_name

@description('Provisioned ACR URL.')
output acr_url string = acr.outputs.acr_url
