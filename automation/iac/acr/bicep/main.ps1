# Dont's forget to run `Connect-AzAccount` before running this

$org = 'taras' # Organization
$workload = 'acr' # Azure Kubernetes Service
$env = 'test' # Shortened below to 't'
$region = 'eastus' # Shortened below to 'eu'

Write-Host 'Retrieving common resource naming template...'
$naming = New-AzSubscriptionDeployment `
  -Location $region `
  -TemplateFile "../../utils/common/bicep/naming.bicep" `
  -org $org `
  -workload $workload `
  -env $env `
  -region $region `
  -ErrorAction Stop

$res_name_template = $naming.Outputs.res_name_template.Value
Write-Host "Resource name template: $res_name_template"

Write-Host 'Provisioning the ACR...'
$acr_deployment = New-AzSubscriptionDeployment `
  -Location $region `
  -TemplateFile "main.bicep" `
  -res_name_template $res_name_template `
  -region $region `
  -ErrorAction Stop

Write-Host "Provisioned Resource Group name: $($acr_deployment.Outputs.rg_name.Value)"
Write-Host "Provisioned ACR name: $($acr_deployment.Outputs.acr_name.Value)"
