# BICEP IaC template for ACR deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this BICEP configuration will provision:
1. Resource Group to contain all other resources (unless override is given)
2. ACR

## Dependencies
1. Powershell
2. Azure Powershell

## Usage

### Provision
To provision the resources, run from this directory in Powershell:
```
Connect-AzAccount
./main.ps1
```

#### Options
1. See the input parameters hardcoded in [main.ps1](main.ps1)
2. See input parameters in [main.bicep](main.bicep)
3. See input parameters in [acr.bicep](acr.bicep)

### Clean Up
1. Unfortunately, it's only possible to delete the whole resource group where the resources were provisioned:
```Remove-AzResourceGroup -Name "<resource_group_name>" -Force```
