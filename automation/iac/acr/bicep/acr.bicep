targetScope = 'resourceGroup'

@description('Application deployment location.')
param region string = 'eastus'

@description('Common resource name template.')
param res_name_template string

resource acr 'Microsoft.ContainerRegistry/registries@2023-01-01-preview' = {
  name: replace(format(res_name_template, 'acr'), '-', '')
  location: region

  sku: {
    name: 'Basic'
  }
}

@description('Provisioned ACR name.')
output acr_name string = acr.name

@description('Provisioned ACR Id.')
output acr_id string = acr.id

@description('Provisioned ACR URL.')
output acr_url string = acr.properties.loginServer
