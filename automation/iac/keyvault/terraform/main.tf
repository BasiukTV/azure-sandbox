terraform {
  required_providers {
    azurerm = "~> 3.100"
    random  = "~> 3.0"
  }
}

provider "azurerm" {
  features {}
}

# Retrieves the IP address of the machine running terraform
data "http" "current_ip" {
  url = "https://api.ipify.org"
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

# Data Source for getting the client id and tenant id
data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "kv" {
  name                = "${local.res_name_prefix}-kv"
  location            = var.loc
  resource_group_name = local.rg_name

  tenant_id = data.azurerm_client_config.current.tenant_id
  sku_name  = "standard"

  soft_delete_retention_days = 7

  # Only allow accesses from authorized Azure Services and current IP address
  network_acls {
    default_action = "Deny"
    bypass         = "AzureServices"
    ip_rules       = [data.http.current_ip.response_body]
  }

  depends_on = [azurerm_resource_group.rg]
}

# Access Policy to allow Terraform working with secrets in the vault
resource "azurerm_key_vault_access_policy" "tf_access" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = data.azurerm_client_config.current.object_id

  secret_permissions = ["Get", "Set", "List", "Delete", "Purge"]
}

# Generate sampel password as a random string, if needed
resource "random_string" "example" {
  length  = 24
  special = true

  count = var.example_secret_value == "" ? 1 : 0
}

# Example KeyVault Secret to be stored in the provisioned Vault
resource "azurerm_key_vault_secret" "secret" {
  name         = "${local.res_name_prefix}-kv-secret"
  value        = var.example_secret_value == "" ? random_string.example[0].result : var.example_secret_value
  key_vault_id = azurerm_key_vault.kv.id

  depends_on = [azurerm_key_vault_access_policy.tf_access]
}

# Add read access on all secrets in the provisioned vault to all given object ids
resource "azurerm_key_vault_access_policy" "read_access" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id    = data.azurerm_client_config.current.tenant_id

  for_each  = var.authorized_client_object_ids
  object_id = each.value

  secret_permissions = ["Get"]
}
