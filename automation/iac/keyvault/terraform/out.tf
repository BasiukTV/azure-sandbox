output "rg_name" {
  description = "Provisioned Resource Group name."
  value       = local.rg_name
}

output "key_vault_name" {
  description = "Provisioned KeyVault name."
  value       = azurerm_key_vault.kv.name
}

output "key_vault_secret_name" {
  description = "Provisioned KeyVault secret name."
  value       = azurerm_key_vault_secret.secret.name
}
