# Variables used for all resources

variable "dept" {
  description = "Department that owns the application."
  type        = string
  default     = "sand" # Sandbox
}

variable "app" {
  description = "Name of the application."
  type        = string
  default     = "keyv" # Key Vault
}

variable "stage" {
  description = "Application deployment stage."
  type        = string
  default     = "test"
}

variable "loc" {
  description = "Application deployment location."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Variables for the Azure Function (and its child services)

variable "authorized_client_object_ids" {
  description = "Object ids of the clients who are authorized to retrieve the secrets from the provisioned vault."
  type        = map(string)
  default     = {}
}

variable "example_secret_value" {
  description = "Example secret value to store in the vault. If none given, random one will be generated."
  type        = string
  default     = ""
  sensitive   = true
}
