/*** Variables used for all resources ***/

# dept  = "sand"   # Department that owns the application.
# app   = "keyv"   # Name of the application.
# stage = "test"   # Application deployment stage.
# loc   = "eastus" # Application deployment location.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Variables for the KeyVault (and its child services) ***/

# authorized_client_object_id = ""

# example_secret_value = ""
