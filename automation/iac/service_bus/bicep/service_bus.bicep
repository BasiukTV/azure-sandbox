targetScope = 'resourceGroup'

@description('Department that owns the application.')
param dept string = 'sand' // Sandbox

@description('Name of the application.')
param app string = 'svcbus' // Service Bus

@description('Application deployment stage.')
param stage string = 'tst' // Test

@description('Application deployment location.')
param loc string = 'eastus' // East US

@description('Number of topic subscriptions to create.')
param subs int = 2

// Common Resource name prefix
var res_name_prfx = '${dept}-${app}-${stage}-${loc}'

// Service Bus Namespace
resource sb_namespace 'Microsoft.ServiceBus/namespaces@2021-11-01' = {
  name: '${res_name_prfx}-sbn'
  location: loc
  sku: { name: 'Standard' }
}

// Service Bus Queue
resource sb_queue 'Microsoft.ServiceBus/namespaces/queues@2021-11-01' = {
  name: '${res_name_prfx}-sbq'
  parent: sb_namespace
  properties: {
    deadLetteringOnMessageExpiration: true
  }
}

// Service Bus Queue Sender Authorization Rule
resource sb_queue_sender_auth 'Microsoft.ServiceBus/namespaces/queues/authorizationRules@2021-11-01' = {
  name: '${res_name_prfx}-sbq-sender-auth'
  parent: sb_queue
  properties: { rights: ['Send'] }
}

// Service Bus Queue Receiver Authorization Rule
resource sb_queue_receiver_auth 'Microsoft.ServiceBus/namespaces/queues/authorizationRules@2021-11-01' = {
  name: '${res_name_prfx}-sbq-receiver-auth'
  parent: sb_queue
  properties: { rights: ['Listen'] }
}

// Service Bus Topic
resource sb_topic 'Microsoft.ServiceBus/namespaces/topics@2021-11-01' = {
  name: '${res_name_prfx}-sbt'
  parent: sb_namespace
}

// Service Bus Queue Sender Authorization Rule
resource sb_topic_sender_auth 'Microsoft.ServiceBus/namespaces/topics/authorizationRules@2021-11-01' = {
  name: '${res_name_prfx}-sbt-sender-auth'
  parent: sb_topic
  properties: { rights: ['Send'] }
}

// Service Bus Topic Subscriptions
resource sb_topic_subs 'Microsoft.ServiceBus/namespaces/topics/subscriptions@2021-11-01' = [
  for i in range(0, subs): {
    name: '${res_name_prfx}-sbt-sub${i}'
    parent: sb_topic
  }
]

resource sb_topic_receivers_auth 'Microsoft.ServiceBus/namespaces/topics/authorizationRules@2021-11-01' = [
  for i in range(0, subs): {
    name: '${res_name_prfx}-sbt-receiver${i}-auth'
    parent: sb_topic
    properties: { rights: ['Listen'] }
  }
]

@description('Name of the provisioned Service Bus Namespace.')
output service_bus_namespace string = sb_namespace.name

@description('Name of the provisioned Service Bus queue.')
output service_bus_queue string = sb_queue.name

@description('Provisioned Service Bus queue sender connection string.')
#disable-next-line outputs-should-not-contain-secrets // This is just a sandbox project, I'm lazy
output service_bus_queue_sender_conn_str string = sb_queue_sender_auth.listKeys().primaryConnectionString

@description('Provisioned Service Bus queue receiver connection string.')
#disable-next-line outputs-should-not-contain-secrets // This is just a sandbox project, I'm lazy
output service_bus_queue_receiver_conn_str string = sb_queue_receiver_auth.listKeys().primaryConnectionString

@description('Name of the provisioned Service Bus topic.')
output service_bus_topic string = sb_topic.name

@description('Provisioned Service Bus topic sender connection string.')
#disable-next-line outputs-should-not-contain-secrets // This is just a sandbox project, I'm lazy
output service_bus_topic_sender_conn_str string = sb_topic_sender_auth.listKeys().primaryConnectionString

@description('Names of the provisioned Service Bus topic subscriptions.')
output service_bus_topic_subscriptions array = [for i in range(0, subs): sb_topic_subs[i].name]

@description('Provisioned Service Bus topic subscribers connection string.')
#disable-next-line outputs-should-not-contain-secrets // This is just a sandbox project, I'm lazy
output service_bus_topic_receiver_conn_str array = [
  for i in range(0, subs): sb_topic_receivers_auth[i].listKeys().primaryConnectionString
]
