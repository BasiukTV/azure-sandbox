using 'main.bicep'

param dept = 'sand' // Sandbox
param app = 'svcbus' // Service Bus
param stage = 'tst' // Test
param loc = 'eastus' // East US

param subs = 2 // Number of subscribtions to create for the topic
