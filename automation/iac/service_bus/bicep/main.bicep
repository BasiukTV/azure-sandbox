/**
 * This provisions a resource group and then calls a module to provision a
 * Service Bus namespace, queue and topic (with multiple subscriber), and
 * finally assigns needed rules to send and receive messages.
 */

targetScope = 'subscription'

@description('Department that owns the application.')
param dept string = 'sand' // Sandbox

@description('Name of the application.')
param app string = 'svcbus' // Service Bus

@description('Application deployment stage.')
param stage string = 'tst' // Test

@description('Application deployment location.')
param loc string = 'eastus' // East US

@description('Number of topic subscriptions to create.')
param subs int = 2

// Common Resource name prefix
var res_name_prfx = '${dept}-${app}-${stage}-${loc}'

resource rg 'Microsoft.Resources/resourceGroups@2024-03-01' = {
  name: '${res_name_prfx}-rg'
  location: loc
}

// Service Bus Module that deploys the resources inside of our resource group
module sb 'service_bus.bicep' = {
  scope: rg
  name: '${res_name_prfx}-sb-module'

  params: {
    dept: dept
    app: app
    stage: stage
    loc: loc

    subs: subs
  }
}
