# BICEP IaC template for Service Bus deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this BICEP configuration will provision:
1. Resource Group to contain all other resources
2. Azure Service Bus Namespace
3. Service Bus Queue
4. Service Bus Topic (and multiple subscriptions)
5. Number of Authorizations rules to send and receive messages to queue and topic

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
az deployment sub create --location <azure_region> --template-file main.bicep --parameters values.bicepparam
```

#### Options
See the [Default Variables Values File](./values.bicepparam). Change the values you wish to modify, and re-run the deployment with the above commands.

### Clean Up
1. Unfortunately, it's only possible to delete the whole resource group where the resources were provisioned:
```
az group delete --name <target-resource-group> --yes --no-wait
```
