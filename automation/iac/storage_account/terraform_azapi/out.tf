output "rg_name" {
  description = "Name of the Resource Group where the resources will be provisioned."
  value       = data.azurerm_resource_group.rg.name
}
