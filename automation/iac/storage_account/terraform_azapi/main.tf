terraform {
  required_providers {
    azurerm = "~> 3.100"
    azapi = {
      source  = "Azure/azapi"
      version = "~> 1.10"
    }
  }
}

provider "azurerm" {
  features {}
}

provider "azapi" {}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

data "azurerm_resource_group" "rg" {
  name = local.rg_name

  depends_on = [azurerm_resource_group.rg]
}

# Deploying Azure Storage Accout throuhg AzureAPI provider
resource "azapi_resource" "sa" {
  type      = "Microsoft.Storage/storageAccounts@2023-01-01"
  name      = replace("${local.res_name_prefix}sa", "-", "")
  parent_id = data.azurerm_resource_group.rg.id

  location = var.loc

  body = {
    sku = {
      name : "Standard_${var.sa_repl_type}"
    }
    kind : "Storage"
    properties : {
      publicNetworkAccess : var.sa_public_network_access ? "Enabled" : "Disabled"
    }
  }
}

