# Variables used for all resources

variable "dept" {
  description = "Department that owns the application."
  type        = string
  default     = "sand" # Sandbox
}

variable "app" {
  description = "Name of the application."
  type        = string
  default     = "satst" # Storage Account Test
}

variable "stage" {
  description = "Application deployment stage."
  type        = string
  default     = "test"
}

variable "loc" {
  description = "Application deployment location."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Variables for the Storage Account (and its child services)

variable "sa_repl_type" {
  description = "Storage Account replication type"
  type        = string
  default     = "LRS"

  validation {
    condition     = contains(["LRS", "ZRS", "GRS", "RAGRS", "GZRS", "RAGZRS"], var.sa_repl_type)
    error_message = "Invalid Storage Account replication type. Allowed values are 'LRS', 'ZRS', 'GRS', 'RAGRS', 'GZRS', or 'RAGZRS'."
  }
}

variable "sa_is_data_lake" {
  description = "Specifies wether the Storage Account should be created as a Data Lake."
  type        = bool
  default     = false
}

variable "sa_file_share_size" {
  description = "Specifies the maximum size (GB) of the Storage Account File Share. No file share will be provisioned if given size is zero (default)."
  type        = number
  default     = 0
}
