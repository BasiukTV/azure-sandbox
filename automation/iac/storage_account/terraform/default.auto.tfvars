/*** Variables used for all resources ***/

# dept  = "sand"    # Department that owns the application.
# app   = "satst" # Name of the application.
# stage = "test"    # Application deployment stage.
# loc   = "eastus"  # Application deployment location.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Variables for the Storage Account (and its child services) ***/

# sa_repl_type = "LRS"
# sa_is_data_lake = false

# sa_file_share_size = 0
