terraform {
  required_providers {
    azurerm = "~> 3.96"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

resource "azurerm_storage_account" "sa" {

  # Storage accounts don't like '-' characters. Name of the resource depends on wether it's a Data Lake or not.
  name                = replace("${local.res_name_prefix}-${var.sa_is_data_lake ? "dl" : "sa"}", "-", "")
  resource_group_name = local.rg_name
  location            = var.loc

  account_tier             = "Standard"
  account_replication_type = var.sa_repl_type
  is_hns_enabled           = var.sa_is_data_lake

  min_tls_version = "TLS1_2"

  depends_on = [azurerm_resource_group.rg]
}

resource "azurerm_storage_container" "blob_container" {
  name                 = "${local.res_name_prefix}-blob-container"
  storage_account_name = azurerm_storage_account.sa.name

  # Only provision the Blob Container if the Storage Account is not a Data Lake
  count = !var.sa_is_data_lake ? 1 : 0
}

resource "azurerm_storage_data_lake_gen2_filesystem" "data_lake_fs" {
  name               = "${local.res_name_prefix}-data-lake-fs"
  storage_account_id = azurerm_storage_account.sa.id

  # Only provision the Data Lake File System if the Storage Account is a Data Lake
  count = var.sa_is_data_lake ? 1 : 0
}

resource "azurerm_storage_share" "file_share" {
  name                 = "${local.res_name_prefix}-file-share"
  storage_account_name = azurerm_storage_account.sa.name

  quota = var.sa_file_share_size # File share maximum size in GB

  count = var.sa_file_share_size != 0 ? 1 : 0 # Only deploy if file share size is non-zero
}
