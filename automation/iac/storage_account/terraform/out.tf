output "sa_name" {
  description = "Provisioned Storage Account name"
  value       = azurerm_storage_account.sa.name
}

output "sa_id" {
  description = "Provisioned Storage Account ID"
  value       = azurerm_storage_account.sa.id
}

output "sa_account_key" {
  description = "Provisioned Storage Account access key."
  value       = azurerm_storage_account.sa.primary_access_key
}

output "sa_blob_container_name" {
  description = "The name of the BLOB container inside of the storage account (if provisioned)."
  value       = !var.sa_is_data_lake ? azurerm_storage_container.blob_container[0].name : ""
}

output "sa_data_lake_fs_name" {
  description = "The name of the Data Lake File System inside of the storage account (if provisioned)."
  value       = var.sa_is_data_lake ? azurerm_storage_data_lake_gen2_filesystem.data_lake_fs[0].name : ""
}

output "sa_data_lake_url" {
  description = "The endpoint of the Storage Account Data Lake."
  value       = var.sa_is_data_lake ? azurerm_storage_account.sa.primary_dfs_endpoint : ""
}

output "sa_file_share_name" {
  description = "The name of the File Share inside of the storage account."
  value       = var.sa_file_share_size != 0 ? azurerm_storage_share.file_share[0].name : ""
}
