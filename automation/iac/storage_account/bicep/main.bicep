@description('Department that owns the application.')
param dept string = 'sand'

@description('Name of the application.')
param app string = 'satst'

@description('Application deployment stage.')
param stage string = 'test'

@description('Application deployment location.')
param loc string = 'eastus'

@description('Whether the provisioned Storage Account will be a Data Lake.')
param is_datalake bool = false

// Common Resource name prefix
var res_name_prfx = '${dept}-${app}-${stage}-${loc}'

resource storage_account 'Microsoft.Storage/storageAccounts@2023-01-01' = {
  // Removes all the '-' in the storage account name
  name: replace('${res_name_prfx}-${is_datalake ? 'dl' : 'sa'}', '-', '')
  location: loc

  kind: is_datalake ? 'StorageV2' : 'Storage'
  sku: {
    name: 'Standard_LRS'
  }
  properties: {
    isHnsEnabled: is_datalake
  }
}

@description('Provisioned Storage Account name.')
output sa_name string = storage_account.name

@description('Provisioned Storage Account ID.')
output sa_id string = storage_account.id

@description('Data Lake FS endpoint, if provisioned.')
output dl_fs_url string = is_datalake ? storage_account.properties.primaryEndpoints.dfs : ''
