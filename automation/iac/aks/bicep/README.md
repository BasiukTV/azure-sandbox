# BICEP IaC template for AKS deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this BICEP configuration will provision:
1. Resource Group to contain all other resources
2. Entra ID group for AKS administrator (currently logged user will be added)
3. Entra ID service principal to be used as AKS administrator for automation
   * <i>Service Principal is not added to the administrator group, and its password is not set.</i>
   * Above is currently have to be done manually.
4. Azure Container Registry to host docker images
4. AKS cluster authorized to pull from provisioned ACR and being administrated by the above Entra Id group

## Dependencies
1. Powershell
2. Azure Powershell

## Usage

### Provision
To provision the resources, run from this directory in Powershell:
```
Connect-AzAccount
./main.ps1
```

#### Options
1. See the input parameters hardcoded in [main.ps1](main.ps1)
2. TODO See aks_config_parameters in [main.bicep](main.bicep)

### Clean Up
1. Unfortunately, it's only possible to delete the whole resource group where the resources were provisioned:
```Remove-AzResourceGroup -Name "<resource_group_name>" -Force```
2. Additionally, you have to delete the AKS admins user group, which is, honestly, easiest to do from the portal
