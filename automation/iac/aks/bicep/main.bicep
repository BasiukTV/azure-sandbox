/**
 * This provisions a resource group and then calls a module to provision an AKS cluster.
 */

targetScope = 'subscription'

@description('Common resource name template to be used for AKS resource deployment.')
param res_name_template string

@description('Application deployment location.')
param region string = 'eastus'

@description('AKS admin group object Id.')
param aks_admins_group_object_id string

@description('Whether to provision new ACR to be used for new AKS cluster.')
param provision_acr bool = true

resource rg 'Microsoft.Resources/resourceGroups@2024-03-01' = {
  name: format(res_name_template, 'rg')
  location: region
}

module acr '../../acr/bicep/acr.bicep' = if (provision_acr) {
  name: 'acr_module'
  scope: rg

  params: {
    res_name_template: res_name_template
  }
}

module aks 'aks.bicep' = {
  scope: rg
  name: 'aks_module'

  params: {
    res_name_template: res_name_template
    region: region
    aks_admins_group_object_id: aks_admins_group_object_id
    system_node_pool_params: {
      name: 'system'
      count: 1
      enableAutoScaling: true
      minCount: 1
      maxCount: 3
      vmSize: 'Standard_D4as_v4'
    }
  }
}

@description('Provisioned Resource Group name.')
output rg_name string = rg.name

@description('Provisioned AKS cluster name.')
output aks_name string = aks.outputs.aks_name

@description('Provisioned ACR name.')
output acr_name string = provision_acr ? acr.outputs.acr_name : ''

@description('Provisioned ACR URL.')
output acr_url string = provision_acr ? acr.outputs.acr_url : ''
