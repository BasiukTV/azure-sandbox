# Dont's forget to run `Connect-AzAccount` before running this

$org = 'taras' # Organization
$workload = 'aks' # Azure Kubernetes Service
$env = 'test' # Shortened below to 't'
$region = 'eastus' # Shortened below to 'eu'

Write-Host 'Retrieving common resource naming template...'
$naming = New-AzSubscriptionDeployment `
  -Location $region `
  -TemplateFile "../../utils/common/bicep/naming.bicep" `
  -org $org `
  -workload $workload `
  -env $env `
  -region $region `
  -ErrorAction Stop

$res_name_template = $naming.Outputs.res_name_template.Value
Write-Host "Resource name template: $res_name_template" -ForegroundColor DarkGreen

Write-Host 'Retrieving ObjectID of the SignedIn user to be used as future AKS cluster admin...'
$aks_admin_object_id = Get-AzADUser -SignedIn | Select-Object -ExpandProperty Id
Write-Host "SignedIn user ObjectId: $aks_admin_object_id" -ForegroundColor DarkGreen

Write-Host 'Creating a service principal to be used as future AKS cluster admin for automation...'
$aks_admin_service_principal = New-AzSubscriptionDeployment `
  -Location $region `
  -TemplateFile "service_principal.bicep" `
  -res_name_template $res_name_template `
  -ErrorAction Stop
$aks_admin_sp_app_name = $aks_admin_service_principal.Outputs.sp_app_name.Value
Write-Host "AKS admin service principal App Name: $aks_admin_sp_app_name" -ForegroundColor DarkGreen
$aks_admin_sp_app_id = $aks_admin_service_principal.Outputs.sp_app_id.Value
Write-Host "AKS admin service principal App Id: $aks_admin_sp_app_id" -ForegroundColor DarkGreen

Write-Host 'Creating AKS admins Entra Id group...'
$aks_admins_group_deployment = New-AzSubscriptionDeployment `
  -Location $region `
  -TemplateFile "admin_group.bicep" `
  -res_name_template $res_name_template `
  -aks_admin_object_id $aks_admin_object_id `
  -ErrorAction Stop
$aks_admins_group_name = $aks_admins_group_deployment.Outputs.aks_admins_group_name.Value
Write-Host "AKS admins Entra Id group name: $aks_admins_group_name" -ForegroundColor DarkGreen

$aks_admins_group_object_id = (Get-AzADGroup -DisplayName $aks_admins_group_name).Id
Write-Host "AKS admins Entra Id group object Id: $aks_admins_group_object_id" -ForegroundColor DarkGreen

Write-Host "Added signedin user to the AKS admins group." -ForegroundColor DarkGreen
Write-Host "It's not currently possible to add service principal: $aks_admin_sp_app_name to the AKS admins group: $aks_admins_group_name automatically. Please do so manually from the Azure Portal." -ForegroundColor DarkYellow

Write-Host 'Provisioning the AKS cluster...'
$aks_deployment = New-AzSubscriptionDeployment `
  -Location $region `
  -TemplateFile "main.bicep" `
  -res_name_template $res_name_template `
  -region $region `
  -aks_admins_group_object_id $aks_admins_group_object_id `
  -ErrorAction Stop

$rg_name = $aks_deployment.Outputs.rg_name.Value
Write-Host "Provisioned Resource Group name: $rg_name" -ForegroundColor DarkGreen
$aks_name = $aks_deployment.Outputs.aks_name.Value
$acr_name = $aks_deployment.Outputs.acr_name.Value
Write-Host "Provisioned AKS cluster name: $aks_name" -ForegroundColor DarkGreen
Write-Host "Provisioned ACR name: $acr_name" -ForegroundColor DarkGreen
Write-Host "Provisioned ACR URL: $($aks_deployment.Outputs.acr_url.Value)" -ForegroundColor DarkGreen

Write-Host 'Authorizing the AKS cluster to pull images from provisioned ACR...'
$null = Set-AzAksCluster -Name $aks_name -ResourceGroupName $rg_name -AcrNameToAttach $acr_name -ErrorAction Stop
Write-Host "Provisioned AKS cluster $aks_name is authorized to pull Docker images from the $acr_name ACR." -ForegroundColor DarkGreen
