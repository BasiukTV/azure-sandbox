@description('Common resource name template to be used for AKS admin group creation.')
param res_name_template string

@description('AKS admin Entra ID Object ID.')
param aks_admin_object_id string

provider microsoftGraph
resource admins 'Microsoft.Graph/groups@v1.0' = {
  displayName: format(res_name_template, 'admins')
  mailEnabled: false
  mailNickname: format(res_name_template, 'admins')
  securityEnabled: true
  uniqueName: format(res_name_template, 'admins')

  members: [aks_admin_object_id]
}

output aks_admins_group_name string = admins.uniqueName
