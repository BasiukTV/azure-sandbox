targetScope = 'resourceGroup'

@description('Application deployment location.')
param region string = 'eastus'

@description('Common resource name template.')
param res_name_template string

@description('AKS admin group object Id.')
param aks_admins_group_object_id string

@description('System node pool node counts.')
param system_node_pool_params object = {
  name: 'system'
  count: 1
  enableAutoScaling: false
  minCount: 1
  maxCount: 2
  vmSize: 'Standard_DS2_v2'
}

resource aks 'Microsoft.ContainerService/managedClusters@2024-06-02-preview' = {
  name: format(res_name_template, 'aks')
  location: region

  sku: {
    name: 'Base'
    tier: 'Free'
  }

  identity: {
    type: 'SystemAssigned'
  }

  properties: {
    dnsPrefix: format(res_name_template, 'aks')

    aadProfile: {
      managed: true
      enableAzureRBAC: true
      adminGroupObjectIDs: [aks_admins_group_object_id]
    }

    agentPoolProfiles: [
      {
        name: system_node_pool_params.name
        mode: 'System'

        osType: 'Linux'
        vmSize: system_node_pool_params.vmSize

        count: system_node_pool_params.count

        enableAutoScaling: system_node_pool_params.enableAutoScaling
        minCount: system_node_pool_params.enableAutoScaling ? system_node_pool_params.minCount : null
        maxCount: system_node_pool_params.enableAutoScaling ? system_node_pool_params.maxCount : null

        nodeLabels: {
          node_pool: 'system'
        }
      }
    ]
  }
}

@description('Provisioned AKS cluster name.')
output aks_name string = aks.name
