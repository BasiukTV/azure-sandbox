@description('Common resource name template to be used for AKS admin group creation.')
param res_name_template string

provider microsoftGraph

// Create a client application registration, setting its credential to the X509 cert public key.
resource app_reg 'Microsoft.Graph/applications@v1.0' = {
  uniqueName: format(res_name_template, 'sp')
  displayName: format(res_name_template, 'sp')
  description: 'Service Principle used for AKS cluster administration.'
}

resource sp 'Microsoft.Graph/servicePrincipals@v1.0' = {
  appId: app_reg.appId
}

@description('Provisioned service principal App Id.')
output sp_app_id string = sp.appId

@description('Provisioned service principal App Name.')
output sp_app_name string = sp.appDisplayName
