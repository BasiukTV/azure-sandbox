/*** Variables used for all resources ***/

# dept  = "sand"   # Department that owns the application.
# app   = "vnet"   # Name of the application.
# stage = "test"   # Application deployment stage.
# loc   = "eastus" # Application deployment location.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Variables for the Storage Account (and its child services) ***/

# vnet_address_space = ["10.0.0.0/16"]

# vnet_subnets = {
#   "subnet0" = {
#     address_prefixes = ["10.0.0.0/24"],
#     allowed_in_ports = [22, 80],
#     allowed_in_current_ip = true,
#     allowed_in_addresses  = [] }
#   "subnet1" = {
#     address_prefixes = ["10.1.0.0/24"],
#     allowed_in_ports = [22, 80],
#     allowed_in_current_ip = true,
#     allowed_in_addresses  = [] }
# }
