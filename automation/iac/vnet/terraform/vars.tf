# Variables used for all resources

variable "dept" {
  description = "Department that owns the application."
  type        = string
  default     = "sand" # Sandbox
}

variable "app" {
  description = "Name of the application."
  type        = string
  default     = "vnet" # Storage Account Test
}

variable "stage" {
  description = "Application deployment stage."
  type        = string
  default     = "test"
}

variable "loc" {
  description = "Application deployment location."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Variables for the Virtual Network (and its child services)

variable "vnet_address_space" {
  description = "Virtual Network address space."
  type        = list(string)
  default     = ["10.0.0.0/16"]
}

# Subnets for the provisioned VNet. Must provide compatible address spaces
variable "vnet_subnets" {
  description = "Virtual Network subnets."
  type = map(object({
    address_prefixes      = list(string)
    allowed_in_ports      = list(number)
    allowed_in_current_ip = bool         # Wether to allow traffic from current IP address
    allowed_in_addresses  = list(string) # Additional list of allowed ingress IP addresses
  }))

  default = {
    "subnet0" = {
      address_prefixes      = ["10.0.0.0/24"],
      allowed_in_ports      = [22, 80],
      allowed_in_current_ip = true,
      allowed_in_addresses  = []
    }
    "subnet1" = {
      address_prefixes      = ["10.0.1.0/24"],
      allowed_in_ports      = [22, 80],
      allowed_in_current_ip = true,
      allowed_in_addresses  = []
    }
  }
}
