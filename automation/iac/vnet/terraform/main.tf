terraform {
  required_providers {
    azurerm = "~> 3.100"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

resource "azurerm_virtual_network" "vnet" {
  name                = "${local.res_name_prefix}-vnet"
  resource_group_name = local.rg_name
  location            = var.loc

  address_space = var.vnet_address_space

  depends_on = [azurerm_resource_group.rg]
}

resource "azurerm_subnet" "subnets" {
  for_each = var.vnet_subnets

  name                 = each.key
  resource_group_name  = local.rg_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = each.value.address_prefixes
}

# Retrieves the IP address of the machine running terraform
data "http" "current_ip" {
  url = "https://api.ipify.org"
}

# Network Security Group intended for the corresponding subnet with some inboud ports open
resource "azurerm_network_security_group" "nsg" {
  for_each = var.vnet_subnets

  name                = "${local.res_name_prefix}-${each.key}-nsg"
  resource_group_name = local.rg_name
  location            = var.loc

  security_rule {
    name                    = "${each.key}-nsg-sec-rule"
    priority                = 100
    direction               = "Inbound"
    access                  = "Allow"
    protocol                = "Tcp"
    source_port_range       = "*"
    destination_port_ranges = each.value.allowed_in_ports
    source_address_prefixes = concat(
      each.value.allowed_in_addresses,
      each.value.allowed_in_current_ip ? [data.http.current_ip.response_body] : []
    )
    destination_address_prefix = "*"
  }

  depends_on = [azurerm_resource_group.rg]
}

# Assigning NSGs to Subnets
resource "azurerm_subnet_network_security_group_association" "subnet-nsg-assoc" {
  for_each = var.vnet_subnets

  subnet_id                 = azurerm_subnet.subnets[each.key].id
  network_security_group_id = azurerm_network_security_group.nsg[each.key].id
}
