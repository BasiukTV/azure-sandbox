output "rg_name" {
  description = "Provisioned Resource Group name."
  value       = local.rg_name
}

output "vnet_name" {
  description = "Provisioned Virtual Network name."
  value       = azurerm_virtual_network.vnet.name
}

output "subnet_ids" {
  description = "Ids of the subnets provisioned."
  value       = [for k, v in azurerm_subnet.subnets : { (k) : v.id }]
}
