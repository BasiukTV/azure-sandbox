# Atmos (Terraform) IaC configuration for Container App deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Additional Dependencies
* Atmos CLI has to be installed - https://atmos.tools/install

## Provisioned Infra
By default this Terraform configuration will provision:
1. [Resource Group](./components/terraform/rg/)
2. [Container Apps Environment](./components/terraform/cae/) (with Consumption-based workload)
3. [Application Insights](./components/terraform/appi/)
4. [Container App](./components/terraform/ca/) (App Insights Open Telemetry tester)

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
export ARM_SUBSCRIPTION_ID="<your_subscription_id>"
atmos workflow cont_apps_apply_test_no_approve --file cont_apps
```

### Testing
Open the ```cont_app_URL``` output produced by the last step of the workflow

#### Options
Variables used during the resource provisioning are configured in:
* [common.yaml](./stacks/catalog/common.yaml)
* [test.yaml](./stacks/deploy/test.yaml)

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
atmos workflow cont_apps_destroy_test_no_approve --file cont_apps
```
2. Delete the Terraform downloaded libraries located in .terraform* directories and files of every component directory
3. Delete the state files located in \*.tfstate\* files of every component directory
4. Delete the plan files located in \*.planfile\* files of every component directory
5. Delete the variable values values located in \*.tfvars.json\* files of every component directory
