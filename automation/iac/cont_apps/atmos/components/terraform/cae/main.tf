resource "azurerm_container_app_environment" "cae" {
  name                = "cae-${var.org}-${var.workload}-${var.environment}-${var.region}"
  location            = var.region
  resource_group_name = var.rg_name

  workload_profile {
    name                  = "Consumption"
    workload_profile_type = "Consumption"
  }
}
