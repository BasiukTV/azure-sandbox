resource "azurerm_resource_group" "rg" {
  name     = "rg-${var.org}-${var.workload}-${var.environment}-${var.region}"
  location = var.region
}
