# Below are some of the varibales used to follow the below resource naming convention
# {type}-{org}-{workload}-{env}-{region}-{instance}

variable "org" {
  description = "Name of the organization that owns the resource."
  type        = string
  default     = "taras"
}

variable "workload" {
  description = "Name of the workload that uses the resource."
  type        = string
  default     = "ca" # Container Apps
}

variable "environment" {
  description = "Environment name."
  type        = string
  default     = "test"
}

variable "region" {
  description = "Region name."
  type        = string
  default     = "eastus"
}
