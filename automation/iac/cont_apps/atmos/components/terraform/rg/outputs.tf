output "rg_name" {
  description = "The name of the provisioned Resource Group."
  value       = azurerm_resource_group.rg.name
}
