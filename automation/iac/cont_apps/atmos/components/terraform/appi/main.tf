resource "azurerm_application_insights" "appi" {
  name                = "appi-${var.org}-${var.workload}-${var.environment}-${var.region}"
  resource_group_name = var.rg_name
  location            = var.region

  application_type = "web"
}
