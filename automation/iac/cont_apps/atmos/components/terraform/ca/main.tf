resource "azurerm_container_app" "ca" {
  name                = "ca-${var.org}-${var.workload}-${var.environment}-${var.region}"
  resource_group_name = var.rg_name

  container_app_environment_id = var.cae_id
  workload_profile_name        = "Consumption"
  revision_mode                = "Single"

  template {
    container {
      name  = "container-${var.org}-${var.workload}-${var.environment}-${var.region}"
      image = var.container_app_image

      cpu    = 0.25
      memory = "0.5Gi"

      env {
        name  = var.env_var_name
        value = var.env_var_value
      }
    }

    min_replicas = 0
    max_replicas = 2
  }

  ingress {
    allow_insecure_connections = true
    external_enabled           = true
    target_port                = 80

    traffic_weight {
      latest_revision = true
      percentage      = 100
    }
  }
}
