# Below are some of the varibales used to follow the below resource naming convention
# {type}-{org}-{workload}-{env}-{region}-{instance}

variable "org" {
  description = "Name of the organization that owns the resource."
  type        = string
  default     = "taras"
}

variable "workload" {
  description = "Name of the workload that uses the resource."
  type        = string
  default     = "ca" # Container Apps
}

variable "environment" {
  description = "Environment name."
  type        = string
  default     = "test"
}

variable "region" {
  description = "Region name."
  type        = string
  default     = "eastus"
}

variable "rg_name" {
  description = "Resource Group name."
  type        = string
}

variable "cae_id" {
  description = "Containers App Environment Id."
  type        = string
}

variable "container_app_image" {
  description = "Container App Image."
  type        = string
}

variable "env_var_name" {
  description = "Container App environmental variable name."
  type        = string
}

variable "env_var_value" {
  description = "Container App environmental variable value."
  type        = string
  sensitive   = true
}
