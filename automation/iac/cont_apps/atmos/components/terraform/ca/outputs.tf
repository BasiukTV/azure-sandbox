output "cont_app_URL" {
  description = "Container App URL."
  value       = azurerm_container_app.ca.latest_revision_fqdn
}
