/**
    Azure Naming Module Usage Assignemnt

    This file demonstrates how to use the Azure Naming Module to generate resource names.
    
    The intended command to provision the resources is:
        $ terraform init
        $ terraform apply -environment=prod --auto-approve

    Scroll down through the file, look for PROBLEM and ANSWER keywords, and complete the code as indicated.
*/

terraform {
  required_version = "~> 1.10"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 4.0"
    }
  }

  backend "local" {
    path = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

variable "org" {
  type        = string
  description = "The name of the organization that owns the workload."
  default     = "taras"
}

variable "workload" {
  type        = string
  description = "The name of the workload."
  default     = "lbtst"
}

variable "environment" {
  type        = string
  description = "The environment name."
  default     = "dev"
}

variable "location" {
  type        = string
  description = "The location for the resources."
  default     = "eastus"
}

variable "backend_addresses" {
  type = map(object({
    address = string
  }))
  description = "The backend addresses for the load balancer."
  default = {
    first  = { address = "10.0.0.5" }
    second = { address = "10.0.0.6" }
  }
}

/**
    PROBLEM 1:
     - List all the resources that will be created by this Terraform configuration.
     - Indicate whether the resource will be a separate resource in Azure or a child resource/setting.

    ANSWER 1:
     - azurerm_resource_group.rg                       (Separate resource)
     - azurerm_public_ip.pip                           (Separate resource)
     - azurerm_lb.lb                                   (Separate resource)
     - azurerm_lb_backend_address_pool.default         (Child resource of azurerm_lb.lb)
     - azurerm_lb_backend_address_pool_address.default (Child resource of the backend pool)
     - azurerm_log_analytics_workspace.laws            (Separate resource)
     - azurerm_monitor_diagnostic_setting.example      (Child resource of the LB)
     - azurerm_lb_probe.http                           (Child resource of the LB)
     - azurerm_lb_rule.default                         (Child resource of the LB)
*/

/**
    PROBLEM 2:
     - List all the resources that this configuration expects to exist already.

    ANSWER 2:
     - azurerm_resource_group.vnet_rg (the “landing zone” RG that already exists)
     - azurerm_virtual_network.vnet   (the pre-existing VNet)
*/

module "prefix_naming" {
  source = "Azure/naming/azurerm"
  prefix = [var.org, var.workload, var.environment, var.location]
}

resource "azurerm_resource_group" "rg" {
  name     = module.prefix_naming.resource_group.name
  location = var.location
}

resource "azurerm_public_ip" "pip" {
  name                = module.prefix_naming.public_ip.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location

  allocation_method = "Static"
}

resource "azurerm_lb" "lb" {
  name                = module.prefix_naming.lb.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location

  sku      = "Standard"
  sku_tier = "Regional"

  frontend_ip_configuration {
    name                 = azurerm_public_ip.pip.name
    public_ip_address_id = azurerm_public_ip.pip.id
  }
}

/*
    PROBLEM 3:
        List the names of the following terraform resources in the Azure portal:
            - azurerm_resource_group.rg.name
            - azurerm_public_ip.pip.name
            - azurerm_lb.lb.name

    ANSWER 3:
     - azurerm_resource_group.rg.name = taras-lbtst-prod-eastus-rg
     - azurerm_public_ip.pip.name     = taras-lbtst-prod-eastus-pip
     - azurerm_lb.lb.name             = taras-lbtst-prod-eastus-lb
*/

module "suffix_naming" {
  source = "Azure/naming/azurerm"
  suffix = [var.environment, var.location, var.org, "apm"]
}

resource "azurerm_log_analytics_workspace" "laws" {
  name                = module.suffix_naming.log_analytics_workspace.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location

  sku = "PerGB2018"
}

resource "azurerm_monitor_diagnostic_setting" "example" {
  name                       = module.suffix_naming.monitor_diagnostic_setting.name
  target_resource_id         = azurerm_lb.lb.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.laws.id

  enabled_log {
    category_group = "allLogs"
  }

  metric {
    category = "AllMetrics"
  }
}

/*
    PROBLEM 4:
        List the names of the following terraform resources in the Azure portal:
            - azurerm_log_analytics_workspace.laws.name
            - azurerm_monitor_diagnostic_setting.example.name

    ANSWER 4:
     - azurerm_log_analytics_workspace.laws.name       = log-prod-eastus-taras-apm
     - azurerm_monitor_diagnostic_setting.example.name = mds-prod-eastus-taras-apm
*/

/*
    PROBLEM 5:
        The following resources already exist in Azure:
            - azurerm_resource_group.vnet_rg.name = taras-lz-eastus-prod-rg
            - azurerm_virtual_network.vnet.name   = taras-lz-eastus-prod-vnet

        Configure the naming module to generate the names of these resources and use them in the data blocks below.
        Use as many exising variables as possible, if there aren't enough, hardcode the missing values.
*/

module "lz_naming" {
  source = "Azure/naming/azurerm"
  prefix = [var.org, "lz", var.location, var.environment /* <ANSWER 5> */]
}

data "azurerm_resource_group" "vnet_rg" {
  name = module.lz_naming.resource_group.name
}

data "azurerm_virtual_network" "vnet" {
  name                = module.lz_naming.virtual_network.name
  resource_group_name = data.azurerm_resource_group.vnet_rg.name
}

resource "azurerm_lb_backend_address_pool" "default" {
  name            = "default-backend-pool"
  loadbalancer_id = azurerm_lb.lb.id
}

resource "azurerm_lb_backend_address_pool_address" "default" {
  for_each = var.backend_addresses

  name                    = "${each.key}-address"
  backend_address_pool_id = azurerm_lb_backend_address_pool.default.id
  virtual_network_id      = data.azurerm_virtual_network.vnet.id
  ip_address              = each.value.address
}

module "sufix_naming" {
  source = "Azure/naming/azurerm"
  suffix = [var.workload, var.environment, var.location]
}

resource "azurerm_lb_probe" "http" {
  name            = "http-probe"
  loadbalancer_id = azurerm_lb.lb.id

  protocol     = "Http"
  request_path = "/"
  port         = 80
}

resource "azurerm_lb_rule" "default" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = "default-rule"

  protocol      = "Tcp"
  frontend_port = 80
  backend_port  = 80

  frontend_ip_configuration_name = azurerm_public_ip.pip.name
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.default.id]
  probe_id                       = azurerm_lb_probe.http.id
}
