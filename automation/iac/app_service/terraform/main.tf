terraform {
  required_providers {
    azurerm = "~> 3.96"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

resource "azurerm_service_plan" "app_svc_plan" {
  name                = "${local.res_name_prefix}-svc-plan"
  resource_group_name = local.rg_name
  location            = var.loc

  os_type  = "Linux"
  sku_name = "F1"

  depends_on = [azurerm_resource_group.rg]
}

resource "azurerm_linux_web_app" "app_svc" {
  name                = "${local.res_name_prefix}-svc"
  resource_group_name = local.rg_name
  location            = var.loc

  service_plan_id = azurerm_service_plan.app_svc_plan.id

  site_config {
    always_on     = false # Needed for Free/F1 Service plans
    http2_enabled = true  # Not sure why this is needed, but Docker deployments seem to need this
  }

  app_settings = {
    # I don't know why this is needed, but without it Docker image deployments don't work
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
  }
}
