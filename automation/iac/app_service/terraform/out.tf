output "rg_name" {
  description = "Name of the resouce group where the resources ended up being provisioned."
  value       = local.rg_name
}

output "app_svc_plan_name" {
  description = "Name of the provisioned App Service plan."
  value       = azurerm_service_plan.app_svc_plan.name
}

output "app_svc_name" {
  description = "Name of the provisioned App Service app."
  value       = azurerm_linux_web_app.app_svc.name
}

output "app_svc_url" {
  description = "URL of the provisioned App Service."
  value       = azurerm_linux_web_app.app_svc.default_hostname
}
