# Terraform IaC template for Stream Analytics deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless override is given)
2. Stream Analytics Job, with Job Query, Job input and Job Output defined
3. Event Hub namespace, and Event Hub (including authorization rule for it) to be used for the above input
4. Azure SQL Server, Azure SQL database, firewall rules to allow connection from other Azure Services and current IP to be used as above Stream Analytics Job output.

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
terraform init
terraform apply
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

#### Outputs
Terraform configuration produces the following useful outputs (with example values):
```
Changes to Outputs:
  + saj_input_eventhub_conn_string  = (sensitive value)
  + saj_input_eventhub_name         = "sand-stantst-test-eastus-ehub"
  + saj_out_mssql_db_admin_login    = "stantsttest"
  + saj_out_mssql_db_admin_password = (sensitive value)
  + saj_out_mssql_db_name           = "sand-stantst-test-eastus-rg-mssql-db"
  + saj_out_mssql_server_name       = "sand-stantst-test-eastus-rg-mssql-server"
```

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
