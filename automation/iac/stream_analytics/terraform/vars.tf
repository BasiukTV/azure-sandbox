# Variables used for all resources

variable "dept" {
  description = "Department that owns the application."
  type        = string
  default     = "sand" # Sandbox
}

variable "app" {
  description = "Name of the application."
  type        = string
  default     = "stantst" # Stream Analytics Test
}

variable "stage" {
  description = "Application deployment stage."
  type        = string
  default     = "test"
}

variable "loc" {
  description = "Application deployment location."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Variables for the Stream Analytics (and its child services)

# Nothing here so far
