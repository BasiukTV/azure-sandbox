terraform {
  required_providers {
    azurerm = "~> 3.96"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

# Stream Analytics Job (SAJ) with query defined
resource "azurerm_stream_analytics_job" "saj" {
  name                = "${local.res_name_prefix}-saj"
  resource_group_name = local.rg_name
  location            = var.loc

  streaming_units = 1

  transformation_query = <<QUERY
      SELECT
          TRY_CAST(COUNT(*) AS nvarchar(max)) AS event_count
          TRY_CAST(AVG(numberField) AS nvarchar(max)) AS avg_num,
          TRY_CAST(MAX(numberField) AS nvarchar(max)) AS max_num,
          TRY_CAST(MIN(numberField) AS nvarchar(max)) AS min_num,
          MIN(TRY_CAST(EventEnqueuedUtcTime AS nvarchar(max))) AS first_event_time,
          MAX(TRY_CAST(EventEnqueuedUtcTime AS nvarchar(max))) AS last_event_time
      INTO [sand-stantst-test-eastus-saj-out-mssql]
      FROM [sand-stantst-test-eastus-saj-in-ehub]
      GROUP BY TumblingWindow(minute, 1)
  QUERY

  depends_on = [azurerm_resource_group.rg[0]]
}

# Module that will deploy Event Hub namespace, Event Hub, and authorisation rule to access it
module "event_hub" {
  source = "../../eventhub/terraform"

  # Make the module use use our deprtment, app, location and stage
  dept  = var.dept
  app   = var.app
  stage = var.stage
  loc   = var.loc

  # Give the module either our RG, or an overrige RG if we use one
  override_rg_name = var.override_rg_name == "" ? azurerm_resource_group.rg[0].name : local.rg_name
}

# SAJ input that uses the Event Hub provisioned by the module above
resource "azurerm_stream_analytics_stream_input_eventhub_v2" "saj_in_ehub" {
  name                    = "${local.res_name_prefix}-saj-in-ehub"
  stream_analytics_job_id = azurerm_stream_analytics_job.saj.id

  servicebus_namespace = module.event_hub.ehub_namespace
  eventhub_name        = module.event_hub.ehub_name

  serialization {
    type     = "Json"
    encoding = "UTF8"
  }

  shared_access_policy_name = module.event_hub.ehub_access_policy
  shared_access_policy_key  = module.event_hub.ehub_primary_key
}

# Module that will deploy Azure SQL database server, database, and firewall rules to allow connection from other Azure resources and current IP address
module "mssql_db" {
  source = "../../mssql/terraform"

  # Make the module use use our deprtment, app, location and stage
  dept  = var.dept
  app   = var.app
  stage = var.stage
  loc   = var.loc

  # Give the module either our RG, or an overrige RG if we use one
  override_rg_name = var.override_rg_name == "" ? azurerm_resource_group.rg[0].name : local.rg_name
}

# SAJ output that uses the MSSQL database provisioned above
resource "azurerm_stream_analytics_output_mssql" "saj_out_mssql_db" {
  name                      = "${local.res_name_prefix}-saj-out-mssql"
  resource_group_name       = local.rg_name
  stream_analytics_job_name = azurerm_stream_analytics_job.saj.name

  server   = module.mssql_db.mssql_server
  database = module.mssql_db.mssql_db
  table    = "${local.res_name_prefix}-saj-out-mssql-db-table"

  user     = module.mssql_db.db_admin_login
  password = module.mssql_db.db_admin_password
}
