output "rg_name" {
  description = "Resource Group name where all the resources are deployed."
  value       = var.override_rg_name == "" ? azurerm_resource_group.rg[0].name : var.override_rg_name
}

output "saj_input_eventhub_name" {
  description = "Event Hub name for Stream Analytics Job input."
  value       = module.event_hub.ehub_name
}

output "saj_input_eventhub_conn_string" {
  description = "Event Hub connection string for Stream Analytics Job input."
  value       = module.event_hub.ehub_connection_string
  sensitive   = true
}

output "saj_out_mssql_server_name" {
  description = "MSSQL DB server name."
  value       = module.mssql_db.mssql_server
}

output "saj_out_mssql_db_name" {
  description = "MSSQL DB name."
  value       = module.mssql_db.mssql_db
}

output "saj_out_mssql_db_admin_login" {
  description = "MSSQL DB admin login for Stream Analytics Job output."
  value       = module.mssql_db.db_admin_login
}

output "saj_out_mssql_db_admin_password" {
  description = "MSSQL DB admin password for Stream Analytics Job output."
  value       = module.mssql_db.db_admin_password
  sensitive   = true
}
