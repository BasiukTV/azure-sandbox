# Below are some of the varibales used to follow the below resource naming convention
# {type}-{org}-{workload}-{env}-{region}-{instance}

output "res_name_template" {
  description = "Resource name template. Intended usage example: format(common.res_name_template, 'rg')"
  value       = "%s-${var.org}-${var.workload}-${local.env_abbrevs[var.env]}-${local.region_abbrevs[var.region]}"
}

output "res_name_template_with_count" {
  description = "Resource name template. Intended usage example: format(common.res_name_template, 'rg', 1)"
  value       = "%s-${var.org}-${var.workload}-${local.env_abbrevs[var.env]}-${local.region_abbrevs[var.region]}-%d"
}
