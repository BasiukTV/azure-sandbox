# Below are some of the varibales used to follow the below resource naming convention
# {type}-{org}-{workload}-{env}-{region}-{instance}

variable "org" {
  description = "Name of the organization that owns the resource."
  type        = string
  default     = "taras"
}

variable "workload" {
  description = "Name of the workload that uses the resource."
  type        = string
  default     = "gls" # GitLab Sandbox
}

variable "env" {
  description = "Environment name."
  type        = string
  default     = "test"

  validation {
    condition     = contains(["test", "qa", "stage", "prod"], var.env)
    error_message = "Environment value: ${var.env} is invalid. Valid values are: ['test', 'qa', 'stage', 'prod']"
  }
}

variable "region" {
  description = "Region name."
  type        = string
  default     = "eastus"

  validation {
    condition     = contains(["eastus", "centralus", "westus"], var.region)
    error_message = "Region value: ${var.region} is invalid. Valid values are: ['eastus', 'centralus', 'westus']"
  }
}
