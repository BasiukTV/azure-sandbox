# Terraform Commons Module

## Common Resource Naming Convention
Helps to follow the common naming convention for your resources:
* ```{type}-{org}-{workload}-{env}-{region}-{instance}```

### Usage Example

```
module "common" {
  source = "../../utils/terraform/common"

  org      = var.org
  workload = var.workload
  env      = var.env
  region   = var.region
}

resource "azurerm_resource_group" "rg" {
  name     = format(module.common.res_name_template, "rg")
  location = var.region
}

resource "azurerm_virtual_network" "vnet" {
  name                = format(module.common.res_name_template_with_count, "vnet", 1)
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.region

  address_space = ["10.0.0.0/16"]
}
```