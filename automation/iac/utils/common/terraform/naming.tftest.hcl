run "test_defaults" {
  command = plan

  assert {
    condition     = output.res_name_template == "%s-taras-gls-t-eu"
    error_message = "Unexpected resource name template for default variable values."
  }

  assert {
    condition     = output.res_name_template_with_count == "%s-taras-gls-t-eu-%d"
    error_message = "Unexpected resource name with count template for default variable values."
  }
}

run "test_overrides" {
  command = plan

  variables {
    org      = "unit"
    workload = "test"
    env      = "prod"
    region   = "westus"
  }

  assert {
    condition     = output.res_name_template == "%s-unit-test-p-wu"
    error_message = "Unexpected resource name template for override variable values."
  }

  assert {
    condition     = output.res_name_template_with_count == "%s-unit-test-p-wu-%d"
    error_message = "Unexpected resource name with count template for override variable values."
  }
}
