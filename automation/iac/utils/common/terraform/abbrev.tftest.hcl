run "test_invalid_abbrev" {
  assert {
    condition     = !contains(keys(local.env_abbrevs), "invalid")
    error_message = "Invalid environment abbreviature should not map to anything."
  }

  assert {
    condition     = !contains(keys(local.region_abbrevs), "invalid")
    error_message = "Invalid region abbreviature should not map to anything."
  }
}

# Note, valid abbreviatures tested in vars.tftest.hcl
