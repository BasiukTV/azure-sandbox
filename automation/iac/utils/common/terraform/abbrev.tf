# List of common abbreviations used in resource naming conventions
locals {
  env_abbrevs = {
    "prod" : "p",
    "qa" : "q",
    "stage" : "s",
    "test" : "t"
  }

  region_abbrevs = {
    "eastus" : "eu",
    "centralus" : "cu",
    "westus" : "wu"
  }
}

output "env_abbrev" {
  value       = local.env_abbrevs[var.env]
  description = "Environment abbreviation/shorthand."
}

output "region_abbrev" {
  value       = local.region_abbrevs[var.region]
  description = "Region abbreviation/shorthand."
}
