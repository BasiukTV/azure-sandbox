run "test_invalid_values" {
  command = plan

  variables {
    env    = "invalid"
    region = "invalid"
  }

  expect_failures = [var.env, var.region]
}

run "test_valid_env_test" {
  command = plan
  variables { env = "test" }
  assert {
    condition     = local.env_abbrevs[var.env] == "t"
    error_message = "Environment ${var.env} has no valid abbreviature."
  }
}

run "test_valid_env_qa" {
  command = plan
  variables { env = "qa" }
  assert {
    condition     = local.env_abbrevs[var.env] == "q"
    error_message = "Environment ${var.env} has no valid abbreviature."
  }
}

run "test_valid_env_stage" {
  command = plan
  variables { env = "stage" }
  assert {
    condition     = local.env_abbrevs[var.env] == "s"
    error_message = "Environment ${var.env} has no valid abbreviature."
  }
}

run "test_valid_env_prod" {
  command = plan
  variables { env = "prod" }
  assert {
    condition     = local.env_abbrevs[var.env] == "p"
    error_message = "Environment ${var.env} has no valid abbreviature."
  }
}

run "test_valid_region_eastus" {
  command = plan
  variables { region = "eastus" }
  assert {
    condition     = local.region_abbrevs[var.region] == "eu"
    error_message = "Environment ${var.region} has no valid abbreviature."
  }
}

run "test_valid_region_centralus" {
  command = plan
  variables { region = "centralus" }
  assert {
    condition     = local.region_abbrevs[var.region] == "cu"
    error_message = "Environment ${var.region} has no valid abbreviature."
  }
}

run "test_valid_region_westus" {
  command = plan
  variables { region = "westus" }
  assert {
    condition     = local.region_abbrevs[var.region] == "wu"
    error_message = "Environment ${var.region} has no valid abbreviature."
  }
}