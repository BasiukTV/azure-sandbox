/* Module for storing common abbreviations used for resource provisioning. */

targetScope = 'subscription'

var env_abbrevs = {
  test: 't'
  qa: 'q'
  stage: 's'
  prod: 'p'
}

@description('Map of environemnt names abbreviatures.')
output env_abbrevs object = env_abbrevs

var region_abbrevs = {
  eastus: 'eu'
  centralus: 'cu'
  westus: 'wu'
}

@description('Map of region names abbreviatures.')
output region_abbrevs object = region_abbrevs
