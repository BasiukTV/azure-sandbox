# BICEP Common Modules

## Common Resource Naming Convention
Helps to follow the common naming convention for your resources:
* ```{type}-{org}-{workload}-{env}-{region}-{instance}```

### Usage Example

```
module naming '../../utils/common/bicep/naming.bicep' = {
  name: 'naming_module'

  params: {
    org: org
    workload: workload
    env: env
    region: region
  }
}

module rg 'rg.bicep' = {
  name: 'rg_module'

  params: {
    rg_name: format(naming.outputs.res_name_template, 'rg')
    region: region
  }
}
```
