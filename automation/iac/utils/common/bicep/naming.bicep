/* Module for establishing the common resource naming schemes. */

targetScope = 'subscription'

@description('Organization that owns the workload.')
param org string = 'taras'

@description('Name of the workload that owns the resources.')
param workload string

@description('Workload deployment environment.')
param env string = 'test'

@description('Environment deployment location.')
param region string = 'eastus'

// Import the abbreviations module
module abbrevs 'abbrev.bicep' = {
  name: 'abbrevs_module'
}

@description('Common resource name template. Follows {type}-{org}-{workload}-{env}-{region} naming scheme. Intended usage: format(res_name_template, \'rg\')')
output res_name_template string = '{0}-${org}-${workload}-${abbrevs.outputs.env_abbrevs[env]}-${abbrevs.outputs.region_abbrevs[region]}'

@description('Common resource name template. Follows {type}-{org}-{workload}-{env}-{region}-{instance} naming scheme. Intended usage: format(res_name_template_with_count, \'rg\', 0)')
output res_name_template_with_count string = '{0}-${org}-${workload}-${abbrevs.outputs.env_abbrevs[env]}-${abbrevs.outputs.region_abbrevs[region]}-{1}'
