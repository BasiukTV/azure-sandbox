# Variables used for all resources

variable "org" {
  description = "Organization that owns the workload."
  type        = string
  default     = "taras"
}

variable "workload" {
  description = "Workload that uses the resources."
  type        = string
  default     = "appi" # Application Insights
}

variable "env" {
  description = "Deployment environment."
  type        = string
  default     = "test"
}

variable "region" {
  description = "Resources deployment region."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Variables for the Application Insights
