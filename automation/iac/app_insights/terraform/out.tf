output "rg_name" {
  description = "Name of the resouce group where the resources ended up being provisioned."
  value       = local.rg_name
}

output "appi_conn_string" {
  description = "Application Insights connection string."
  sensitive   = true
  value       = azurerm_application_insights.appi.connection_string
}
