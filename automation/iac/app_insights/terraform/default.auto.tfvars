/*** Variables used for all resources ***/

# org      = "taras"  # Organization that owns the workload.
# workload = "appi"   # Workload that uses the resources.
# env      = "test"   # Deployment environment.
# region   = "eastus" # Resources deployment region.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Variables for the Application Insights ***/
