terraform {
  required_version = "~> 1.9"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 4.0"
    }
  }

  backend "local" {}
}

provider "azurerm" {
  resource_provider_registrations = "none"

  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

module "common" {
  source = "../../utils/common/terraform/"

  org      = var.org
  workload = var.workload
  env      = var.env
  region   = var.region
}

locals {
  # Expects resource_type to format a resource name according to the following naming convention
  # {resource_type}-{org}-{workload}-{env}-{region}
  res_name_template = module.common.res_name_template

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? format(module.common.res_name_template, "rg") : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.region

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

# Application insights resource itself
resource "azurerm_application_insights" "appi" {
  name                = format(local.res_name_template, "appi")
  resource_group_name = local.rg_name
  location            = var.region

  application_type = "other"

  depends_on = [azurerm_resource_group.rg]
}
