output "private_ip" {
  description = "Private IP address of the provisioned VM."
  value       = azurerm_network_interface.nic.ip_configuration[0].private_ip_address
}

output "public_ip" {
  description = "Public IP address of the provisioned VM (if requested)."
  value       = var.with_pip ? azurerm_public_ip.pip[0].ip_address : null
}
