# Public IP address of the provisioned machine (if requested).
resource "azurerm_public_ip" "pip" {
  count = var.with_pip ? 1 : 0

  name                = module.naming.public_ip.name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.region

  sku               = "Basic"
  allocation_method = "Dynamic"
}
