#######################
# Naming Prefix Parts #
#######################

# org      = "taras"
# workload = "vmtst" # VM Test
# env      = "test"
# region   = "eastus"
# instance = null

###########################
# Override Resource Group #
###########################

# override_rg = null

##################
# Network Config #
##################

# with_pip = true

# existing_vnet_config = null

### Below provided here for reference on the above value. ###

# variable "existing_vnet_config" {
#   description = "Configuration of existing VNet and subnet to use for VM provisioning."
#   type = object({
#     vnet_rg_name = string
#     vnet_name    = string
#     subnet_name  = string
#   })
#
#   default = null
# }

# vnet_address_space   = ["10.0.0.0/16"]
# subnet_address_space = ["10.0.0.0/24"]

#############
# VM Config #
#############

# vm_size  = "Standard_B1s"
# vm_admin = "taras"

#################
# VM Extensions #
#################

# vm_extensions = {
#   "install_nginx" = "sudo apt-get update && sudo apt-get install -y nginx"
# }
