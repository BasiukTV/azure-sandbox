terraform {
  required_providers {
    azurerm = {
      version = ">= 4.0"
      source  = "hashicorp/azurerm"
    }
  }

  required_version = ">= 1.9"

  backend "local" {}
}

provider "azurerm" {
  features {}
}

module "common" {
  source = "../../utils/common/terraform"

  org      = var.org
  workload = var.workload
  env      = var.env
  region   = var.region
}

# Naming convention we will use for our resources:
#     {type}-{org}-{workload}-{env}-{region}-{instance}
module "naming" {
  source = "Azure/naming/azurerm"
  suffix = concat([
    var.org,
    var.workload,
    module.common.env_abbrev,
    module.common.region_abbrev
  ], var.instance == null ? [] : [var.instance])
}

resource "azurerm_resource_group" "rg" {
  count = var.override_rg == null ? 1 : 0

  name     = module.naming.resource_group.name
  location = var.region
}

# Data source to be used either for the RG that we provisioned or for the override one
data "azurerm_resource_group" "rg" {
  name = var.override_rg == null ? azurerm_resource_group.rg[0].name : var.override_rg
}
