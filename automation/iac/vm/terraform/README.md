# Terraform IaC template for Azure VM Provisioning

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless an override is given).
2. VNet and subnet (unless an override is given).
3. (by default) Public IP Address.
4. Network Interface Card.
5. Network Security Group (attached to NIC, allowing SSH and HTTP connections from current IP address)
6. (Ubuntu) Linux VM with SSH certification for connection.
7. Custom VM Extensions (NGinx by default).

## Usage

### Provision
To provision the resources, run from this directory:
```
export ARM_SUBSCRIPTION_ID=<your_azure_subscription_id>
az login
terraform init
terraform apply

# If need to know the VM IP addresses
terraform output private_ip
terraform output public_ip
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
