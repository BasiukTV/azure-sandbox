# Retrieves the IP address of the machine running terraform
data "http" "current_ip" {
  url = "https://api.ipify.org"
}

# Network Security Group and its rules.
resource "azurerm_network_security_group" "nsg" {
  name                = module.naming.network_security_group.name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.region

  security_rule {
    name                       = "AllowSSHFromCurrentIP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = data.http.current_ip.response_body # Allow current IP to SSH onto the machine
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "AllowHTTPFromCurrentIP"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = data.http.current_ip.response_body # Allow current IP to HTTP the machine
    destination_address_prefix = "*"
  }
}
