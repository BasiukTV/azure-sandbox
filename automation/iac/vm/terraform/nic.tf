# Network interface card and its association with a Network Security Group

resource "azurerm_network_interface" "nic" {
  name                = module.naming.network_interface.name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.region

  ip_configuration {
    name                          = "default"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = var.with_pip ? azurerm_public_ip.pip[0].id : null
    subnet_id                     = data.azurerm_subnet.subnet.id
  }
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}
