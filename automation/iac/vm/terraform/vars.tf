#######################
# Naming Prefix Parts #
#######################

variable "org" {
  description = "Name of the organization that owns the resource."
  type        = string
  default     = "taras"
}

variable "workload" {
  description = "Name of the workload that uses the resource."
  type        = string
  default     = "vmtst" # VM Test
}

variable "env" {
  description = "Environment name."
  type        = string
  default     = "test"
}

variable "region" {
  description = "Region name."
  type        = string
  default     = "eastus"
}

variable "instance" {
  description = "Index/Count of this VM instance to be included in the naming convention."
  type        = number
  default     = null
}

###########################
# Override Resource Group #
###########################

variable "override_rg" {
  description = "Name of the existing resource group to use instead of provisioning a new one."
  type        = string
  default     = null
}

##################
# Network Config #
##################

variable "with_pip" {
  description = "Wether to deploy the VM with a public IP address."
  type        = bool
  default     = true
}

variable "existing_vnet_config" {
  description = "Configuration of existing VNet and subnet to use for VM provisioning."
  type = object({
    vnet_rg_name = string
    vnet_name    = string
    subnet_name  = string
  })

  default = null
}

variable "vnet_address_space" {
  description = "Address space of the provisioned VNet. If existing_vnet_config is given, this variable will be ignored."
  type        = list(string)
  default     = ["10.0.0.0/16"]
}

variable "subnet_address_space" {
  description = "Address space of the provisioned subnet. If existing_vnet_config is given, this variable will be ignored."
  type        = list(string)
  default     = ["10.0.0.0/24"]
}

#############
# VM Config #
#############

variable "vm_size" {
  description = "Provisioned VM size."
  type        = string
  default     = "Standard_B1s"
}

variable "vm_admin" {
  description = "Provisioned VM admin username."
  type        = string
  default     = "taras"
}

#################
# VM Extensions #
#################

variable "vm_extensions" {
  description = "Set of extensions to install on the provisioned VM."
  type        = map(string)
  default = {
    "install_nginx" = "sudo apt-get update && sudo apt-get install -y nginx"
  }
}
