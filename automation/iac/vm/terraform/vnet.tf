// Virtual Network Resource (if requested)
resource "azurerm_virtual_network" "vnet" {
  count = var.existing_vnet_config == null ? 1 : 0

  name                = module.naming.virtual_network.name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.region

  address_space = var.vnet_address_space
}

// Virtual Network Subnet Resource (if requested)
resource "azurerm_subnet" "subnet" {
  count = var.existing_vnet_config == null ? 1 : 0

  name                 = module.naming.subnet.name
  resource_group_name  = data.azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet[0].name

  address_prefixes = var.subnet_address_space
}

# Data source for the subnet (either provisioned one, or existing one) where the VM will be provisioned.
data "azurerm_subnet" "subnet" {
  name                 = var.existing_vnet_config != null ? var.existing_vnet_config.subnet_name : azurerm_subnet.subnet[0].name
  resource_group_name  = var.existing_vnet_config != null ? var.existing_vnet_config.vnet_rg_name : azurerm_subnet.subnet[0].resource_group_name
  virtual_network_name = var.existing_vnet_config != null ? var.existing_vnet_config.vnet_name : azurerm_subnet.subnet[0].virtual_network_name
}
