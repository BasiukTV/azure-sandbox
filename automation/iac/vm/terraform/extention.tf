# Installs given list of VM extensions
resource "azurerm_virtual_machine_extension" "extensions" {
  for_each = var.vm_extensions

  name                       = each.key
  virtual_machine_id         = azurerm_linux_virtual_machine.vm.id
  publisher                  = "Microsoft.Azure.Extensions"
  type                       = "CustomScript"
  type_handler_version       = "2.1"
  auto_upgrade_minor_version = true

  settings = <<SETTINGS
  {
    "commandToExecute": "${each.value}"
  }
  SETTINGS
}
