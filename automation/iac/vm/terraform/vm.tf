resource "azurerm_linux_virtual_machine" "vm" {
  name                = module.naming.linux_virtual_machine.name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = var.region

  size = var.vm_size

  admin_username = var.vm_admin
  admin_ssh_key {
    username   = var.vm_admin
    public_key = file("${path.module}/terraform_ssh_key.pub")
  }

  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  os_disk {
    name                 = module.naming.managed_disk.name
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
}
