# Terraform IaC template for Azure NAT Gateway Testing

## Notes
1. Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).
2. VNet/SubNet is assumed to be (or provisioned) in the same RG where everything else is. 

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless an override is given).
2. VNet and subnet (unless an override is given).
3. Standard Public IP Address.
4. NAT Gateway (associated with the subnet, and the Public IP address).

## Usage

### Provision
To provision the resources, run from this directory:
```
export ARM_SUBSCRIPTION_ID=<your_azure_subscription_id>
az login
terraform init
terraform apply
terraform output
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
