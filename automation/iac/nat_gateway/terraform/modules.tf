// Using the bellow module for common repository abbreviations
module "commons" {
  source = "../../utils/common/terraform"

  org      = var.org
  workload = var.workload
  env      = var.env
  region   = var.region
}

// Azure Terraform resource naming module. Please see the module's documentation for more information.
module "naming" {
  source = "Azure/naming/azurerm"

  // Below should generate a name like "rg-taras-natgtw-t-eu"
  suffix = [var.org, var.workload, module.commons.env_abbrev, module.commons.region_abbrev]
}
