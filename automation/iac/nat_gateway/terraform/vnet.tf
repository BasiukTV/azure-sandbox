resource "azurerm_virtual_network" "vnet" {
  count = var.override_vnet == null ? 1 : 0

  name                = module.naming.virtual_network.name
  location            = var.region
  resource_group_name = data.azurerm_resource_group.rg.name

  address_space = [var.vnet_cidr]
}

data "azurerm_virtual_network" "vnet" {
  name                = var.override_vnet == null ? azurerm_virtual_network.vnet[0].name : var.override_vnet
  resource_group_name = data.azurerm_resource_group.rg.name
}

