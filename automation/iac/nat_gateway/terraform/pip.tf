resource "azurerm_public_ip" "pip" {
  name                = module.naming.public_ip.name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku                 = "Standard"
}