output "rg_name" {
  description = "Name of the resource group name where the NAT Gateway is provisioned."
  value       = data.azurerm_resource_group.rg.name
}

output "nat_gateway_name" {
  description = "Name of the NAT Gateway provisioned."
  value       = azurerm_nat_gateway.natgtw.name
}

output "nat_gateway_outbound_ip" {
  description = "Provisioned NAT Gateway's outbound IP address."
  value       = azurerm_public_ip.pip.ip_address
}