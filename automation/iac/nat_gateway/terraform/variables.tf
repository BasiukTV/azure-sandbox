#######################
# Naming Prefix Parts #
#######################

variable "org" {
  description = "Name of the organization that owns the resource."
  type        = string
  default     = "taras"
}

variable "workload" {
  description = "Name of the workload that uses the resource."
  type        = string
  default     = "natgtw" # NAT Gateway
}

variable "env" {
  description = "Environment name."
  type        = string
  default     = "test"
}

variable "region" {
  description = "Region name."
  type        = string
  default     = "eastus"
}

###########################
# Override Resource Group #
###########################

variable "override_rg" {
  description = "Name of the existing resource group to use instead of provisioning a new one."
  type        = string
  default     = null # null means no override
}

##########################
# VNet/SubNet Parameters #
##########################

variable "vnet_cidr" {
  description = "CIDR block (address space) for the VNet. If override_vnet is set, this value is ignored."
  type        = string
  default     = "10.1.0.0/16"
}

variable "subnet_cidr" {
  description = "CIDR block (address space) for the Subnet. If override_subnet is set, this value is ignored."
  type        = string
  default     = "10.1.1.0/24"
}

variable "override_vnet" {
  description = "Name of the existing VNet to use instead of provisioning a new one."
  type        = string
  default     = null # null means no override
}

variable "override_subnet" {
  description = "Name of the existing subnet to use instead of provisioning a new one."
  type        = string
  default     = null # null means no override
}
