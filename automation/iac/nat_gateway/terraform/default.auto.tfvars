#######################
# Naming Prefix Parts #
#######################

# org      = "taras"
# workload = "natgtw" # NAT Gateway
# env      = "test"
# region   = "eastus"

###########################
# Override Resource Group #
###########################

# override_rg = null

########################
# Override VNet/SubNet #
########################

# override_vnet = null
# override_subnet = null