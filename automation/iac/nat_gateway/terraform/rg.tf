resource "azurerm_resource_group" "rg" {
  count = var.override_rg == null ? 1 : 0

  name     = module.naming.resource_group.name
  location = var.region
}

data "azurerm_resource_group" "rg" {
  name = var.override_rg == null ? azurerm_resource_group.rg[0].name : var.override_rg
}