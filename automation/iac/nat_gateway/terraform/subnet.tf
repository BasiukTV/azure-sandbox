resource "azurerm_subnet" "subnet" {
  count = var.override_subnet == null ? 1 : 0

  name                 = module.naming.subnet.name
  resource_group_name  = data.azurerm_resource_group.rg.name
  virtual_network_name = data.azurerm_virtual_network.vnet.name
  address_prefixes     = [var.subnet_cidr]
}

data "azurerm_subnet" "subnet" {
  name                 = var.override_subnet == null ? azurerm_subnet.subnet[0].name : var.override_subnet
  virtual_network_name = data.azurerm_virtual_network.vnet.name
  resource_group_name  = data.azurerm_resource_group.rg.name
}