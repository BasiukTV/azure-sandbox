terraform {
  required_providers {
    azurerm = "~> 3.100"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

# Private DNZ zone to that will own the records in the VNet
resource "azurerm_private_dns_zone" "pdns_zone" {
  name                = "privatelink.${var.service_endpoint}"
  resource_group_name = local.rg_name

  depends_on = [azurerm_resource_group.rg]
}

# Linking the Private DNS zone to the Virtual Network
resource "azurerm_private_dns_zone_virtual_network_link" "pdnszlink" {
  name                  = "${local.res_name_prefix}-pdns-zone-vnet-link"
  resource_group_name   = local.rg_name
  private_dns_zone_name = azurerm_private_dns_zone.pdns_zone.name
  virtual_network_id    = var.vnet_id
}

# Private endpoint resource itself
# This relies on Azure creating CNAME record from common service FQDN to private DNS zone
# Private DNS zone then creates A record to map resource FQDN to private IP address
resource "azurerm_private_endpoint" "pep" {
  name                = "${local.res_name_prefix}-pep"
  location            = var.loc
  resource_group_name = local.rg_name
  subnet_id           = var.subnet_id # The subnet private endpoint will get an IP in

  custom_network_interface_name = "${local.res_name_prefix}-pep-nic"

  private_service_connection {
    name                           = "${local.res_name_prefix}-private-svc-conn"
    is_manual_connection           = false
    private_connection_resource_id = var.private_svc_conn.private_conn_res_id
    subresource_names              = var.private_svc_conn.subresource_names
  }

  private_dns_zone_group {
    name                 = "${local.res_name_prefix}-private-dnz-zone-group"
    private_dns_zone_ids = [azurerm_private_dns_zone.pdns_zone.id]
  }

  depends_on = [azurerm_resource_group.rg]
}
