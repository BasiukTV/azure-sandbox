# Terraform IaC template for Private Endpoint deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless override is given)
2. Private DNS zone connected to a given VNet
3. Private Endpoint for the given resource, NIC with Private IP address

## Usage

### Configure .tfvars file
1. See the [Default Variables Values File](./default.auto.tfvars).
2. Uncomment the line you wish to modify, change the value.
3. Update the values for VNet, subnet, service endpoint, resource id

### Provision
To provision the resources, run from this directory:
```
az login
terraform init
terraform apply
```

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
