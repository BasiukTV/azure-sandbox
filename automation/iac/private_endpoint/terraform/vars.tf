# Variables used for all resources

variable "dept" {
  description = "Department that owns the application."
  type        = string
  default     = "sand" # Sandbox
}

variable "app" {
  description = "Name of the application."
  type        = string
  default     = "pep" # Private Endpoint
}

variable "stage" {
  description = "Application deployment stage."
  type        = string
  default     = "test"
}

variable "loc" {
  description = "Application deployment location."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Variables for the Private Endpoint

variable "vnet_id" {
  description = "Virtual Network ID to deploy private DNS zone and attach the private endpoint to."
  type        = string
}

variable "subnet_id" {
  description = "Subnet ID to attach the private endpoint to."
  type        = string
}

variable "service_endpoint" {
  description = "Common URL for the service we will provision the private endpoint for."
  type        = string
}

variable "private_svc_conn" {
  description = "Attributes of the resource to establish the private endpoint for."
  type = object({
    private_conn_res_id = string       # Resource Id to connect to
    subresource_names   = list(string) # Subresource names, if any
  })
}
