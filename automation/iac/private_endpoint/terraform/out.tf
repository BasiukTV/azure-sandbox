output "rg_name" {
  description = "Provisioned Resource Group name."
  value       = local.rg_name
}

output "pep_pip" {
  description = "Private IP address of the provisioned private endpoint."
  value       = azurerm_private_endpoint.pep.private_service_connection[0].private_ip_address
}
