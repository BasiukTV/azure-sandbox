/*** Variables used for all resources ***/

# dept  = "sand"   # Department that owns the application.
# app   = "pep"    # Name of the application.
# stage = "test"   # Application deployment stage.
# loc   = "eastus" # Application deployment location.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Variables for the Private Endpoint (see different options below) ***/

/*** Azure Storage Account PEP Value examples ***/

vnet_id   = "/subscriptions/<subscription_id>/resourceGroups/<rg_name>/providers/Microsoft.Network/virtualNetworks/<vnet_name>"
subnet_id = "/subscriptions/<subscription_id>/resourceGroups/<rg_name>/providers/Microsoft.Network/virtualNetworks/<vnet_name>/subnets/<subnet_name>"

service_endpoint = "blob.core.windows.net"

private_svc_conn = {
  private_conn_res_id = "/subscriptions/<subscription_id>/resourceGroups/<rg_name>/providers/Microsoft.Storage/storageAccounts/<sa_name>"
  subresource_names   = ["blob"]
}


/*** Azure Function App PEP Value examples ***/

# vnet_id   = "/subscriptions/<subscription_id>/resourceGroups/<resource_group_name>/providers/Microsoft.Network/virtualNetworks/<virtual_network_name>"
# subnet_id = "/subscriptions/<subscription_id>/resourceGroups/<resource_group_name>/providers/Microsoft.Network/virtualNetworks/<virtual_network_name>/subnets/<subnet_name>"
#
# service_endpoint = "azurewebsites.net"
#
# private_svc_conn = {
#   private_conn_res_id = "/subscriptions/<subscription_id>/resourceGroups/<resource_group_name>/providers/Microsoft.Web/sites/<function_app_name>"
#   subresource_names   = ["sites"]
# }
