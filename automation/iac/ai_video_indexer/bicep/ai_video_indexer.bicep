/**
 * This provisions a Azure AI Video Indexer Service and a storage account for its use.
 */

@description('Department that owns the application.')
param dept string = 'sand'

@description('Name of the application.')
param app string = 'aivindxr'

@description('Application deployment stage.')
param stage string = 'test'

@description('Application deployment location.')
param loc string = 'eastus'

// Common Resource name prefix
var res_name_prfx = '${dept}-${app}-${stage}-${loc}'

// We provision a storage account via another module.
module sa '../../storage_account/bicep/main.bicep' = {
  name: '${res_name_prfx}-sa-module'

  params: {
    dept: dept
    app: app
    stage: stage
    loc: loc
  }
}

// Actually deploy Azure AI Video Indexer Service now
resource ai_vindxr 'Microsoft.VideoIndexer/accounts@2024-01-01' = {
  name: '${res_name_prfx}-ai-vindxr'
  location: loc

  identity: {
    type: 'SystemAssigned'
  }

  properties: {
    storageServices: {
      resourceId: sa.outputs.sa_id
    }
  }
}

@description('Managed Identity of the AI Video Indexer Service')
output ai_vindxr_managed_identity string = ai_vindxr.identity.principalId

@description('Name of the Storage account used by AI Video Indexer Service')
output sa_name string = sa.outputs.sa_name
