/**
 * This provisions a resource group and then calls a module to provision an
 * Azure AI Video Indexer Service (which provisions a Storage Account), and
 * finally assigns needed roles for the Video Indexer Service.
 */

targetScope = 'subscription'

@description('Department that owns the application.')
param dept string = 'sand'

@description('Name of the application.')
param app string = 'aivindxr'

@description('Application deployment stage.')
param stage string = 'test'

@description('Application deployment location.')
param loc string = 'eastus'

// Common Resource name prefix
var res_name_prfx = '${dept}-${app}-${stage}-${loc}'

resource rg 'Microsoft.Resources/resourceGroups@2023-07-01' = {
  name: '${res_name_prfx}-rg'
  location: loc
}

// This provisions a Azure AI Video Indexer Service (and a storage acoount for it)
module ai_vindxr 'ai_video_indexer.bicep' = {
  name: '${res_name_prfx}-vindxr-module'
  scope: rg

  params: {
    dept: dept
    app: app
    stage: stage
    loc: loc
  }
}

// This assigns required role to Azure AI Video Indexer over the storage account
module role_assignments 'role_assignments.bicep' = {
  name: '${res_name_prfx}-role-assignments-module'
  scope: rg

  params: {
    ai_vindxr_managed_identity: ai_vindxr.outputs.ai_vindxr_managed_identity
    sa_name: ai_vindxr.outputs.sa_name
  }
}
