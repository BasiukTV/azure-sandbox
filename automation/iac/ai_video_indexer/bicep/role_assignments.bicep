/**
 * This assigns a Storage Account BLOB Contributor role for AI Video Indexer Service
 * over the Storage Account that was provisioned for it.
 */

@description('Managed Identity of the AI Video Indexer Service')
param ai_vindxr_managed_identity string

@description('Name of the Storage account used by AI Video Indexer Service')
param sa_name string

@description('Storage Account BLOB Contributor role defenition Id.')
var sa_contr_role_def_id = 'ba92f5b4-2d11-453d-a403-e96b0029c9fe'

// Just a data source for already existing Storage Accout to be used for the role assignment scope later
resource sa 'Microsoft.Storage/storageAccounts@2023-01-01' existing = {
  name: sa_name
}

// Actually doing the role assignment now
resource role_assign 'Microsoft.Authorization/roleAssignments@2022-04-01' = {
  name: guid('${ai_vindxr_managed_identity}${sa.id}${sa_contr_role_def_id}')
  scope: sa
  properties: {
    principalId: ai_vindxr_managed_identity
    roleDefinitionId: subscriptionResourceId('Microsoft.Authorization/roleDefinitions', sa_contr_role_def_id)
  }
}
