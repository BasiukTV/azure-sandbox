# BICEP IaC template for Storage Account deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this BICEP configuration will provision:
1. Resource Group to contain all other resources
2. Azure AI Video Indexer Service
3. Azure Storage Account to be used by Video Indexer Service
4. Storage Account Blob Contributor role assignment for Video Indexer on the Storage Account

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
az deployment sub create --location <azure_region> --template-file main.bicep --parameters @values.json
```

#### Options
See the [Default Variables Values File](./values.json). Change the values you wish to modify, and run the deployment with the below commands instead.
```
az login
az deployment group create --resource-group <target-resource-group> --template-file main.bicep --parameters @values.json
```

### Clean Up
1. Unfortunately, it's only possible to delete the whole resource group where the resources were provisioned:
```
az group delete --name <target-resource-group> --yes --no-wait
```
