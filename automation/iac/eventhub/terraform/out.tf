output "ehub_namespace" {
  description = "Name of the provisioned Event Hub Namespace."
  value       = azurerm_eventhub_namespace.ehub_namespace.name
}

output "ehub_name" {
  description = "Name of the provisioned Event Hub."
  value       = azurerm_eventhub.ehub.name
}

output "ehub_access_policy" {
  description = "Name of the provisioned Event Hub access policy."
  value       = azurerm_eventhub_authorization_rule.auth_rule.name
}

output "ehub_connection_string" {
  description = "Provisioned Event Hub access policy primary connection string."
  value       = azurerm_eventhub_authorization_rule.auth_rule.primary_connection_string
  sensitive   = true
}

output "ehub_primary_key" {
  description = "Provisioned Event Hub access policy primary key."
  value       = azurerm_eventhub_authorization_rule.auth_rule.primary_key
  sensitive   = true
}
