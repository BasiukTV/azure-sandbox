/*** Variables used for all resources ***/

# dept  = "sand"    # Department that owns the application.
# app   = "ehubtst" # Name of the application.
# stage = "test"    # Application deployment stage.
# loc   = "eastus"  # Application deployment location.

# override_rg_name = "" # Use this to explicitly set the Resource Group where the resources will be deployed.

/*** Even Hub Namespace Variables ***/

# ehub_namespace_sku      = "Standard" # Event Hub Namespace SKU.
# ehub_namespace_capacity = 1          # Event Hub Namespace capacity.

/*** Even Hub Namespace Variables ***/

# ehub_partitions    = 5 # Event Hub partitions.
# ehub_msg_retention = 1 # Event Hub message retention (days).
