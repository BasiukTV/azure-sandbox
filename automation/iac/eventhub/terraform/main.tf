terraform {
  required_providers {
    azurerm = "~> 3.96"
  }
}

provider "azurerm" {
  features {}
}

locals {
  # Resource name prefix that helps to form the final resource name
  res_name_prefix = "${var.dept}-${var.app}-${var.stage}-${var.loc}"

  # Resource group name for all the resources, unless override is provided
  rg_name = var.override_rg_name == "" ? "${local.res_name_prefix}-rg" : var.override_rg_name
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.loc

  # Only provision the RG if no override was given
  count = var.override_rg_name == "" ? 1 : 0
}

resource "azurerm_eventhub_namespace" "ehub_namespace" {
  resource_group_name = local.rg_name
  name                = "${local.res_name_prefix}-ehub-namespace"
  location            = var.loc

  sku      = var.ehub_namespace_sku
  capacity = var.ehub_namespace_capacity

  depends_on = [azurerm_resource_group.rg]
}

# Module to deploy a BLOB container used for Event Hub event capture
module "captur_BLOB_container" {
  source = "../../storage_account/terraform"

  # Make the module use use our deprtment, app, location and stage
  dept  = var.dept
  app   = var.app
  stage = var.stage
  loc   = var.loc

  # Give the module either our RG, or an overrige RG if we use one
  override_rg_name = var.override_rg_name == "" ? azurerm_resource_group.rg[0].name : var.override_rg_name
}

resource "azurerm_eventhub" "ehub" {
  resource_group_name = local.rg_name
  name                = "${local.res_name_prefix}-ehub"

  namespace_name    = azurerm_eventhub_namespace.ehub_namespace.name
  partition_count   = var.ehub_partitions
  message_retention = var.ehub_msg_retention

  # Event Hub capture configuration
  capture_description {
    enabled             = true
    encoding            = "Avro"
    skip_empty_archives = true

    destination {
      name                = "EventHubArchive.AzureBlockBlob"
      storage_account_id  = module.captur_BLOB_container.sa_id
      blob_container_name = module.captur_BLOB_container.sa_blob_container_name
      archive_name_format = "{Namespace}/{EventHub}/{PartitionId}/{Year}/{Month}/{Day}/{Hour}/{Minute}/{Second}"
    }
  }
}

resource "azurerm_eventhub_authorization_rule" "auth_rule" {
  name                = "${local.res_name_prefix}-ehub-auth-rule"
  namespace_name      = azurerm_eventhub_namespace.ehub_namespace.name
  eventhub_name       = azurerm_eventhub.ehub.name
  resource_group_name = local.rg_name
  listen              = true
  send                = true
  manage              = true
}

resource "azurerm_eventhub_namespace_schema_group" "schema_group" {
  name                 = "${local.res_name_prefix}-ehub-schema-group"
  namespace_id         = azurerm_eventhub_namespace.ehub_namespace.id
  schema_compatibility = "Forward"
  schema_type          = "Avro"
}
