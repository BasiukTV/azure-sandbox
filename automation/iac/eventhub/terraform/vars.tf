# Variables used for all resources

variable "dept" {
  description = "Department that owns the application."
  type        = string
  default     = "sand"
}

variable "app" {
  description = "Name of the application."
  type        = string
  default     = "ehubtst"
}

variable "stage" {
  description = "Application deployment stage."
  type        = string
  default     = "test"
}

variable "loc" {
  description = "Application deployment location."
  type        = string
  default     = "eastus"
}

variable "override_rg_name" {
  description = "Use this to explicitly set the Resource Group where the resources will be deployed."
  type        = string
  default     = ""
}

# Even Hub Namespace Variables

variable "ehub_namespace_sku" {
  description = "Event Hub Namespace SKU."
  type        = string
  default     = "Standard"

  validation {
    condition     = contains(["Basic", "Standard", "Premium"], var.ehub_namespace_sku)
    error_message = "Invalid SKU value. Allowed values are 'Basic', 'Standard', or 'Premium'."
  }
}

variable "ehub_namespace_capacity" {
  description = "Event Hub Namespace capacity."
  type        = number
  default     = 1
}

# Even Hub Namespace Variables

variable "ehub_partitions" {
  description = "Event Hub partitions."
  type        = number
  default     = 5
}

variable "ehub_msg_retention" {
  description = "Event Hub message retention (days)."
  type        = number
  default     = 1
}
