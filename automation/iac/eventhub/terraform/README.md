# Terraform IaC template for Event Hub deployment

## Notes
Currently only meant to be run locally, while logged into Azure interactively (via a browser pop-up).

## Provisioned Infra
By default this Terraform configuration will provision:
1. Resource Group (unless override is given)
2. Event Hub Namespace
3. Event Hub (inside of the namespace)
4. Storage Account where the Event Hub events will be captured in Avro format.
5. Event Hub Schema Group
6. \* There is also [example schema file](../../../../data/eventhub/schema/test_schema.json) available, but you have to upload it inside of the schema group manually, as the only automated way available now is an API call, and I'm too lazy to write a script for that.

## Usage

### Provision
To provision the resources, run from this directory:
```
az login
terraform init
terraform apply
```

#### Options
See the [Default Variables Values File](./default.auto.tfvars). Uncomment the line you wish to modify, change the value and re-provision the infrastructure by running the steps above.

### Clean Up
1. To remove the resources in the cloud, run from this directory:
```
terraform destroy
```
2. Delete the Terraform downloaded libraries located in .terraform* directories
3. Delete the state files located in \*.tfstate\* files
