# azure-sandbox

## Purpose
Sandbox for playing around with Azure resources deployed with IaC tools and some simple applications that use them.

## Usage
1. Navigate to the [IaC Directory](./automation/iac/), find a folder with the resource you want to explore, and follow the instructions in README.md to provision it.
2. Navigate to the [Apps Directory](./apps/), find a folder with the same name as in #1, and follow the instructions in README.md to run it.

## Requirements
1. [Active Azure Subscription](https://azure.microsoft.com/en-us/free/)
2. [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli) or [Azure Powershell](https://learn.microsoft.com/en-us/powershell/azure/install-azure-powershell)
3. If the resource provisioning template of interest located in the [IaC Folder](./automation/iac/) is a:
  * Terraform folder -> you will need [Terraform](https://developer.hashicorp.com/terraform/install)
4. If the sample app of interest located in the [Apps Folder](./apps/) is a:
  * Python folder -> you will need [Python](https://www.python.org/downloads/)

## Currently Explored Resources

| Resource                    | IaC Templates                                                                                                                                                                      | Apps                                                                                                                                                  |
| --------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| ACR                         | [BICEP](./automation/iac/acr/bicep/) / <s>Terraform</s>                                                                                                                            | -                                                                                                                                                     |
| AI Video indexer            | [BICEP](./automation/iac/ai_video_indexer/bicep/) / <s>Terraform</s>                                                                                                               | -                                                                                                                                                     |
| AKS                         | [BICEP](./automation/iac/aks/bicep/) / <s>Terraform</s>                                                                                                                            | -                                                                                                                                                     |
| App Insights                | <s>BICEP</s> / [Terraform](./automation/iac/app_insights/terraform/)                                                                                                               | [Python Flask OpenTelemetry Tester](./apps/app_insights/python/)                                                                                      |
| App Service                 | <s>BICEP</s> / [Terraform](./automation/iac/app_service/terraform/)                                                                                                                | [Azure CLI/Powershell Scripts](./apps/app_service/docker/) for Dockerized Apps                                                                        |
| Container Apps              | <s>BICEP</s> / <s>Terraform</s> / [Atmos Terraform](./automation/iac/cont_apps/atmos/)                                                                                             | Same as for App Insights testing. See [README](./automation/iac/cont_apps/atmos/README.md)                                                            |
| Event Hub                   | <s>BICEP</s> / [Terraform](./automation/iac/eventhub/terraform/)                                                                                                                   | [Python Event Producer](./apps/eventhub/python/event_producer.py) / [Python Event Consumer](./apps/eventhub/python/event_consumer.py)                 |
| Email Communication Service | <s>BICEP</s> / <s>Terraform</s>                                                                                                                                                    | [Python Weather API Forecast Email Sender](./apps/email_comm_svc/python/)                                                                             |
| Function                    | <s>BICEP</s> / [Terraform](./automation/iac/function/terraform/)                                                                                                                   | [Example Python Function](./apps/function/python/)                                                                                                    |
| KeyVault                    | <s>BICEP</s> / [Terraform](./automation/iac/keyvault/terraform/)                                                                                                                   | -                                                                                                                                                     |
| Load Balancer               | <s>BICEP</s> / [Terraform](./automation/iac/load_balancer/terraform/)                                                                                                              | [Example Express.js App](./apps/load_balancer/express_js/status_app/) / [Example Load Testing Pyhon Script](./apps/load_balancer/python/load_tester/) |
| MSSQL Database              | <s>BICEP</s> / [Terraform](./automation/iac/mssql/terraform/)                                                                                                                      | -                                                                                                                                                     |
| NAT Gateway                 | <s>BICEP</s> / [Terraform](./automation/iac/nat_gateway/terraform/)                                                                                                                | -                                                                                                                                                     |
| Private Endpoint            | <s>BICEP</s> / [Terraform](./automation/iac/private_endpoint/terraform/)                                                                                                           | -                                                                                                                                                     |
| Service Bus                 | [BICEP](./automation/iac/service_bus/bicep/) / <s>Terraform</s>                                                                                                                    | [Dotnet App Samples](./apps/service_bus/dotnet/)                                                                                                      |
| Storage Account / Data Lake | [BICEP](./automation/iac/storage_account/bicep/) / [Terraform](./automation/iac/storage_account/terraform/) / [Terraform AzAPI](./automation/iac/storage_account/terraform_azapi/) | -                                                                                                                                                     |
| Stream Analytics Job        | <s>BICEP</s> / [Terraform](./automation/iac/stream_analytics/terraform/)                                                                                                           | [Python Event Hub to Azure SQL Analytics Job](./apps/stream_analytics/python/README.md)                                                               |
| Virtual Machine             | <s>BICEP</s> / [Terraform](./automation/iac/vm/terraform/)                                                                                                                         | -                                                                                                                                                     |
| Virtual Network             | <s>BICEP</s> / [Terraform](./automation/iac/vnet/terraform/)                                                                                                                       | -                                                                                                                                                     |
| Virtual Network Peering     | <s>BICEP</s> / [Terraform](./automation/iac/vnet_peering/terraform/)                                                                                                               | -                                                                                                                                                     |

## Desired State Configuration (DSC)

### Ansible

1. [Windows Server DSC with Ansible](./automation/dsc/windows/ansible/)
