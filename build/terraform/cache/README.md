# Purpose

This directory is used to store the Terraform cache files during the pipeline runs. Nothing else should be stored here.